﻿namespace RecentlyVisitedSites
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.googleLabel = new System.Windows.Forms.Label();
            this.rankenLabel = new System.Windows.Forms.Label();
            this.microsoftLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(12, 9);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(89, 13);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "www.google.com";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            this.linkLabel1.MouseEnter += new System.EventHandler(this.linkLabel1_MouseEnter);
            this.linkLabel1.MouseLeave += new System.EventHandler(this.linkLabel1_MouseLeave);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(187, 9);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(112, 13);
            this.linkLabel2.TabIndex = 1;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "www.insideranken.org";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            this.linkLabel2.MouseEnter += new System.EventHandler(this.linkLabel2_MouseEnter);
            this.linkLabel2.MouseLeave += new System.EventHandler(this.linkLabel2_MouseLeave);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(382, 9);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(99, 13);
            this.linkLabel3.TabIndex = 2;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "www.microsoft.com";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            this.linkLabel3.MouseEnter += new System.EventHandler(this.linkLabel3_MouseEnter);
            this.linkLabel3.MouseLeave += new System.EventHandler(this.linkLabel3_MouseLeave);
            // 
            // googleLabel
            // 
            this.googleLabel.AutoSize = true;
            this.googleLabel.Location = new System.Drawing.Point(12, 48);
            this.googleLabel.Name = "googleLabel";
            this.googleLabel.Size = new System.Drawing.Size(121, 13);
            this.googleLabel.TabIndex = 3;
            this.googleLabel.Text = "Google\'s search engine.";
            // 
            // rankenLabel
            // 
            this.rankenLabel.AutoSize = true;
            this.rankenLabel.Location = new System.Drawing.Point(187, 48);
            this.rankenLabel.Name = "rankenLabel";
            this.rankenLabel.Size = new System.Drawing.Size(169, 13);
            this.rankenLabel.TabIndex = 4;
            this.rankenLabel.Text = "Ranken\'s student access website.";
            // 
            // microsoftLabel
            // 
            this.microsoftLabel.AutoSize = true;
            this.microsoftLabel.Location = new System.Drawing.Point(382, 48);
            this.microsoftLabel.Name = "microsoftLabel";
            this.microsoftLabel.Size = new System.Drawing.Size(116, 26);
            this.microsoftLabel.TabIndex = 5;
            this.microsoftLabel.Text = "Microsoft\'s home page \r\nfor their website.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 351);
            this.Controls.Add(this.microsoftLabel);
            this.Controls.Add(this.rankenLabel);
            this.Controls.Add(this.googleLabel);
            this.Controls.Add(this.linkLabel3);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.Label googleLabel;
        private System.Windows.Forms.Label rankenLabel;
        private System.Windows.Forms.Label microsoftLabel;
    }
}

