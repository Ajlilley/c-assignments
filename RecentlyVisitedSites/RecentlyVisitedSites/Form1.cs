﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RecentlyVisitedSites
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.linkLabel1.LinkVisited = true;

            System.Diagnostics.Process.Start("http://www.google.com");

            this.linkLabel1.Location = new System.Drawing.Point(12, 9);
            this.linkLabel2.Location = new System.Drawing.Point(187, 9);
            this.linkLabel3.Location = new System.Drawing.Point(382, 9);

            this.googleLabel.Location = new System.Drawing.Point(12, 48);
            this.rankenLabel.Location = new System.Drawing.Point(187, 48);
            this.microsoftLabel.Location = new System.Drawing.Point(382, 48);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.linkLabel2.LinkVisited = true;

            System.Diagnostics.Process.Start("http://www.insideranken.org");

            this.linkLabel2.Location = new System.Drawing.Point(12, 9);
            this.linkLabel1.Location = new System.Drawing.Point(187, 9);
            this.linkLabel3.Location = new System.Drawing.Point(382, 9);

            this.rankenLabel.Location = new System.Drawing.Point(12, 48);
            this.googleLabel.Location = new System.Drawing.Point(187, 48);
            this.microsoftLabel.Location = new System.Drawing.Point(382, 48);
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.linkLabel3.LinkVisited = true;

            System.Diagnostics.Process.Start("http://www.microsoft.com");

            this.linkLabel3.Location = new System.Drawing.Point(12, 9);
            this.linkLabel2.Location = new System.Drawing.Point(187, 9);
            this.linkLabel1.Location = new System.Drawing.Point(382, 9);

            this.microsoftLabel.Location = new System.Drawing.Point(12, 48);
            this.rankenLabel.Location = new System.Drawing.Point(187, 48);
            this.googleLabel.Location = new System.Drawing.Point(382, 48);
        }

        private void linkLabel1_MouseEnter(object sender, EventArgs e)
        {
            googleLabel.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            googleLabel.Hide();
            rankenLabel.Hide();
            microsoftLabel.Hide();
        }

        private void linkLabel1_Leave(object sender, EventArgs e)
        {
            googleLabel.Hide();
        }

        private void linkLabel1_MouseLeave(object sender, EventArgs e)
        {
            googleLabel.Hide();
        }

        private void linkLabel2_MouseEnter(object sender, EventArgs e)
        {
            rankenLabel.Show();
        }

        private void linkLabel2_MouseLeave(object sender, EventArgs e)
        {
            rankenLabel.Hide();
        }

        private void linkLabel3_MouseEnter(object sender, EventArgs e)
        {
            microsoftLabel.Show();
        }

        private void linkLabel3_MouseLeave(object sender, EventArgs e)
        {
            microsoftLabel.Hide();
        }
    }
}
