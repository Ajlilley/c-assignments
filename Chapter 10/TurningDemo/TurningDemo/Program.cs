﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurningDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Corner c = new Corner();
            Leaf l = new Leaf();
            Pancake p = new Pancake();


            ITurnable[] irs = new ITurnable[] { c, l, p };

            foreach (ITurnable ir in irs)
            {
                UseTurn(c);
                UseTurn(l);
                UseTurn(p);
                Console.ReadLine();
            }
        }

        private static void UseTurn(ITurnable ir)
        {
            ir.Turn();
        }
    }
}
