﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurningDemo
{
    public class Pancake : ITurnable
    {
        public void Turn()
        {
            Console.WriteLine("You turn the pancake and add maple syurp.");
        }
    }
}
