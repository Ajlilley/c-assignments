﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecoveringDemo
{
    class FootballPlayer : IRecoverable
    {
        public void Recover()
        {
            Console.WriteLine("The audience is cheering me on and I am getting better.");

        }
    }
}
