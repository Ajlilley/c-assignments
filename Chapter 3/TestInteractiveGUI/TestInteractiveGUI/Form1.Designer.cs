﻿namespace TestInteractiveGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.test1Label = new System.Windows.Forms.Label();
            this.test2Label = new System.Windows.Forms.Label();
            this.test3Label = new System.Windows.Forms.Label();
            this.test4Label = new System.Windows.Forms.Label();
            this.test5Label = new System.Windows.Forms.Label();
            this.test1TextBox = new System.Windows.Forms.TextBox();
            this.test2TextBox = new System.Windows.Forms.TextBox();
            this.test3TextBox = new System.Windows.Forms.TextBox();
            this.test4TextBox = new System.Windows.Forms.TextBox();
            this.test5TextBox = new System.Windows.Forms.TextBox();
            this.testAverageLabel = new System.Windows.Forms.Label();
            this.testAverageTotalLabel = new System.Windows.Forms.Label();
            this.testAverageButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // test1Label
            // 
            this.test1Label.AutoSize = true;
            this.test1Label.Location = new System.Drawing.Point(66, 19);
            this.test1Label.Name = "test1Label";
            this.test1Label.Size = new System.Drawing.Size(73, 13);
            this.test1Label.TabIndex = 0;
            this.test1Label.Text = "Test score #1";
            // 
            // test2Label
            // 
            this.test2Label.AutoSize = true;
            this.test2Label.Location = new System.Drawing.Point(66, 53);
            this.test2Label.Name = "test2Label";
            this.test2Label.Size = new System.Drawing.Size(73, 13);
            this.test2Label.TabIndex = 1;
            this.test2Label.Text = "Test score #2";
            // 
            // test3Label
            // 
            this.test3Label.AutoSize = true;
            this.test3Label.Location = new System.Drawing.Point(66, 91);
            this.test3Label.Name = "test3Label";
            this.test3Label.Size = new System.Drawing.Size(73, 13);
            this.test3Label.TabIndex = 2;
            this.test3Label.Text = "Test score #3";
            // 
            // test4Label
            // 
            this.test4Label.AutoSize = true;
            this.test4Label.Location = new System.Drawing.Point(66, 125);
            this.test4Label.Name = "test4Label";
            this.test4Label.Size = new System.Drawing.Size(73, 13);
            this.test4Label.TabIndex = 3;
            this.test4Label.Text = "Test score #4";
            // 
            // test5Label
            // 
            this.test5Label.AutoSize = true;
            this.test5Label.Location = new System.Drawing.Point(66, 164);
            this.test5Label.Name = "test5Label";
            this.test5Label.Size = new System.Drawing.Size(73, 13);
            this.test5Label.TabIndex = 4;
            this.test5Label.Text = "Test score #5";
            // 
            // test1TextBox
            // 
            this.test1TextBox.Location = new System.Drawing.Point(208, 19);
            this.test1TextBox.Name = "test1TextBox";
            this.test1TextBox.Size = new System.Drawing.Size(100, 20);
            this.test1TextBox.TabIndex = 5;
            // 
            // test2TextBox
            // 
            this.test2TextBox.Location = new System.Drawing.Point(208, 53);
            this.test2TextBox.Name = "test2TextBox";
            this.test2TextBox.Size = new System.Drawing.Size(100, 20);
            this.test2TextBox.TabIndex = 6;
            // 
            // test3TextBox
            // 
            this.test3TextBox.Location = new System.Drawing.Point(208, 91);
            this.test3TextBox.Name = "test3TextBox";
            this.test3TextBox.Size = new System.Drawing.Size(100, 20);
            this.test3TextBox.TabIndex = 7;
            // 
            // test4TextBox
            // 
            this.test4TextBox.Location = new System.Drawing.Point(208, 125);
            this.test4TextBox.Name = "test4TextBox";
            this.test4TextBox.Size = new System.Drawing.Size(100, 20);
            this.test4TextBox.TabIndex = 8;
            // 
            // test5TextBox
            // 
            this.test5TextBox.Location = new System.Drawing.Point(208, 164);
            this.test5TextBox.Name = "test5TextBox";
            this.test5TextBox.Size = new System.Drawing.Size(100, 20);
            this.test5TextBox.TabIndex = 9;
            // 
            // testAverageLabel
            // 
            this.testAverageLabel.AutoSize = true;
            this.testAverageLabel.Location = new System.Drawing.Point(66, 216);
            this.testAverageLabel.Name = "testAverageLabel";
            this.testAverageLabel.Size = new System.Drawing.Size(104, 13);
            this.testAverageLabel.TabIndex = 10;
            this.testAverageLabel.Text = "Your test average is:";
            // 
            // testAverageTotalLabel
            // 
            this.testAverageTotalLabel.AutoSize = true;
            this.testAverageTotalLabel.Location = new System.Drawing.Point(205, 216);
            this.testAverageTotalLabel.Name = "testAverageTotalLabel";
            this.testAverageTotalLabel.Size = new System.Drawing.Size(0, 13);
            this.testAverageTotalLabel.TabIndex = 11;
            // 
            // testAverageButton
            // 
            this.testAverageButton.Location = new System.Drawing.Point(69, 264);
            this.testAverageButton.Name = "testAverageButton";
            this.testAverageButton.Size = new System.Drawing.Size(239, 37);
            this.testAverageButton.TabIndex = 12;
            this.testAverageButton.Text = "Calculate your test average";
            this.testAverageButton.UseVisualStyleBackColor = true;
            this.testAverageButton.Click += new System.EventHandler(this.testAverageButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 346);
            this.Controls.Add(this.testAverageButton);
            this.Controls.Add(this.testAverageTotalLabel);
            this.Controls.Add(this.testAverageLabel);
            this.Controls.Add(this.test5TextBox);
            this.Controls.Add(this.test4TextBox);
            this.Controls.Add(this.test3TextBox);
            this.Controls.Add(this.test2TextBox);
            this.Controls.Add(this.test1TextBox);
            this.Controls.Add(this.test5Label);
            this.Controls.Add(this.test4Label);
            this.Controls.Add(this.test3Label);
            this.Controls.Add(this.test2Label);
            this.Controls.Add(this.test1Label);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label test1Label;
        private System.Windows.Forms.Label test2Label;
        private System.Windows.Forms.Label test3Label;
        private System.Windows.Forms.Label test4Label;
        private System.Windows.Forms.Label test5Label;
        private System.Windows.Forms.TextBox test1TextBox;
        private System.Windows.Forms.TextBox test2TextBox;
        private System.Windows.Forms.TextBox test3TextBox;
        private System.Windows.Forms.TextBox test4TextBox;
        private System.Windows.Forms.TextBox test5TextBox;
        private System.Windows.Forms.Label testAverageLabel;
        private System.Windows.Forms.Label testAverageTotalLabel;
        private System.Windows.Forms.Button testAverageButton;
    }
}

