﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestInteractiveGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void testAverageButton_Click(object sender, EventArgs e)
        {
            double test1;
            double test2;
            double test3;
            double test4;
            double test5;
            test1 = Convert.ToDouble(test1TextBox.Text);
            test2 = Convert.ToDouble(test2TextBox.Text);
            test3 = Convert.ToDouble(test3TextBox.Text);
            test4 = Convert.ToDouble(test4TextBox.Text);
            test5 = Convert.ToDouble(test5TextBox.Text);
            double testAverage = (test1 + test2 + test3 + test4 + test5) / 5;
            testAverageTotalLabel.Text = testAverage.ToString("F2") + "%";
        }
    }
}
