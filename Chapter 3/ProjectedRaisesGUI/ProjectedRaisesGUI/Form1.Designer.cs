﻿namespace ProjectedRaisesGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.salaryLabel1 = new System.Windows.Forms.Label();
            this.salaryTextBox1 = new System.Windows.Forms.TextBox();
            this.nexyYearSalaryLlabel1 = new System.Windows.Forms.Label();
            this.salaryButton1 = new System.Windows.Forms.Button();
            this.newSalaryLabel1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // salaryLabel1
            // 
            this.salaryLabel1.AutoSize = true;
            this.salaryLabel1.Location = new System.Drawing.Point(28, 53);
            this.salaryLabel1.Name = "salaryLabel1";
            this.salaryLabel1.Size = new System.Drawing.Size(141, 13);
            this.salaryLabel1.TabIndex = 0;
            this.salaryLabel1.Text = "Enter in an employees salary";
            // 
            // salaryTextBox1
            // 
            this.salaryTextBox1.Location = new System.Drawing.Point(271, 53);
            this.salaryTextBox1.Name = "salaryTextBox1";
            this.salaryTextBox1.Size = new System.Drawing.Size(121, 20);
            this.salaryTextBox1.TabIndex = 1;
            // 
            // nexyYearSalaryLlabel1
            // 
            this.nexyYearSalaryLlabel1.AutoSize = true;
            this.nexyYearSalaryLlabel1.Location = new System.Drawing.Point(28, 125);
            this.nexyYearSalaryLlabel1.Name = "nexyYearSalaryLlabel1";
            this.nexyYearSalaryLlabel1.Size = new System.Drawing.Size(190, 13);
            this.nexyYearSalaryLlabel1.TabIndex = 2;
            this.nexyYearSalaryLlabel1.Text = "This employee\'s salary next year will be";
            // 
            // salaryButton1
            // 
            this.salaryButton1.Location = new System.Drawing.Point(31, 192);
            this.salaryButton1.Name = "salaryButton1";
            this.salaryButton1.Size = new System.Drawing.Size(187, 32);
            this.salaryButton1.TabIndex = 4;
            this.salaryButton1.Text = "Calculate next year salary";
            this.salaryButton1.UseVisualStyleBackColor = true;
            this.salaryButton1.Click += new System.EventHandler(this.salaryButton1_Click);
            // 
            // newSalaryLabel1
            // 
            this.newSalaryLabel1.AutoSize = true;
            this.newSalaryLabel1.Location = new System.Drawing.Point(268, 125);
            this.newSalaryLabel1.Name = "newSalaryLabel1";
            this.newSalaryLabel1.Size = new System.Drawing.Size(0, 13);
            this.newSalaryLabel1.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 337);
            this.Controls.Add(this.newSalaryLabel1);
            this.Controls.Add(this.salaryButton1);
            this.Controls.Add(this.nexyYearSalaryLlabel1);
            this.Controls.Add(this.salaryTextBox1);
            this.Controls.Add(this.salaryLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label salaryLabel1;
        private System.Windows.Forms.TextBox salaryTextBox1;
        private System.Windows.Forms.Label nexyYearSalaryLlabel1;
        private System.Windows.Forms.Button salaryButton1;
        private System.Windows.Forms.Label newSalaryLabel1;
    }
}

