﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectedRaisesGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void salaryButton1_Click(object sender, EventArgs e)
        {
            double currentSalary;
            double raise;
            double raisePercent = 0.04;
            double newSalary;
            currentSalary = Convert.ToDouble(salaryTextBox1.Text);
            raise = currentSalary * raisePercent;
            newSalary = currentSalary + raise;
            newSalaryLabel1.Text = newSalary.ToString("C");
        }
    }
}
