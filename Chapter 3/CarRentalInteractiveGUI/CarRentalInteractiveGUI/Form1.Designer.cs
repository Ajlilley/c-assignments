﻿namespace CarRentalInteractiveGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.daysRentedLabel1 = new System.Windows.Forms.Label();
            this.milesDrivenLabel2 = new System.Windows.Forms.Label();
            this.daysRentedtextBox1 = new System.Windows.Forms.TextBox();
            this.milesDrivenTextBox2 = new System.Windows.Forms.TextBox();
            this.rentalFeeLabel3 = new System.Windows.Forms.Label();
            this.rentalTotalLabel4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // daysRentedLabel1
            // 
            this.daysRentedLabel1.AutoSize = true;
            this.daysRentedLabel1.Location = new System.Drawing.Point(74, 48);
            this.daysRentedLabel1.Name = "daysRentedLabel1";
            this.daysRentedLabel1.Size = new System.Drawing.Size(183, 13);
            this.daysRentedLabel1.TabIndex = 0;
            this.daysRentedLabel1.Text = "How many days did you rent this car?";
            // 
            // milesDrivenLabel2
            // 
            this.milesDrivenLabel2.AutoSize = true;
            this.milesDrivenLabel2.Location = new System.Drawing.Point(74, 97);
            this.milesDrivenLabel2.Name = "milesDrivenLabel2";
            this.milesDrivenLabel2.Size = new System.Drawing.Size(189, 13);
            this.milesDrivenLabel2.TabIndex = 1;
            this.milesDrivenLabel2.Text = "How many miles did you drive this car?";
            // 
            // daysRentedtextBox1
            // 
            this.daysRentedtextBox1.Location = new System.Drawing.Point(319, 48);
            this.daysRentedtextBox1.Name = "daysRentedtextBox1";
            this.daysRentedtextBox1.Size = new System.Drawing.Size(100, 20);
            this.daysRentedtextBox1.TabIndex = 2;
            // 
            // milesDrivenTextBox2
            // 
            this.milesDrivenTextBox2.Location = new System.Drawing.Point(319, 97);
            this.milesDrivenTextBox2.Name = "milesDrivenTextBox2";
            this.milesDrivenTextBox2.Size = new System.Drawing.Size(100, 20);
            this.milesDrivenTextBox2.TabIndex = 3;
            // 
            // rentalFeeLabel3
            // 
            this.rentalFeeLabel3.AutoSize = true;
            this.rentalFeeLabel3.Location = new System.Drawing.Point(74, 151);
            this.rentalFeeLabel3.Name = "rentalFeeLabel3";
            this.rentalFeeLabel3.Size = new System.Drawing.Size(112, 13);
            this.rentalFeeLabel3.TabIndex = 4;
            this.rentalFeeLabel3.Text = "Your total rental fee is:";
            // 
            // rentalTotalLabel4
            // 
            this.rentalTotalLabel4.AutoSize = true;
            this.rentalTotalLabel4.Location = new System.Drawing.Point(316, 151);
            this.rentalTotalLabel4.Name = "rentalTotalLabel4";
            this.rentalTotalLabel4.Size = new System.Drawing.Size(0, 13);
            this.rentalTotalLabel4.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(77, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "Calculate your rental fee";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 311);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rentalTotalLabel4);
            this.Controls.Add(this.rentalFeeLabel3);
            this.Controls.Add(this.milesDrivenTextBox2);
            this.Controls.Add(this.daysRentedtextBox1);
            this.Controls.Add(this.milesDrivenLabel2);
            this.Controls.Add(this.daysRentedLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label daysRentedLabel1;
        private System.Windows.Forms.Label milesDrivenLabel2;
        private System.Windows.Forms.TextBox daysRentedtextBox1;
        private System.Windows.Forms.TextBox milesDrivenTextBox2;
        private System.Windows.Forms.Label rentalFeeLabel3;
        private System.Windows.Forms.Label rentalTotalLabel4;
        private System.Windows.Forms.Button button1;
    }
}

