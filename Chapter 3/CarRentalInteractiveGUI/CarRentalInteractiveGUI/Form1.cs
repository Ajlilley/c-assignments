﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRentalInteractiveGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int daysRented;
            double miles;
            daysRented = Convert.ToInt32(daysRentedtextBox1.Text);
            miles = Convert.ToDouble(milesDrivenTextBox2.Text);
            double totalCharge = daysRented * 20 + miles * .25;
            rentalTotalLabel4.Text = totalCharge.ToString("C");
        }
    }
}
