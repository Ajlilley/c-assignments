﻿namespace EggInteractiveGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.enterLabel1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.calculateButton1 = new System.Windows.Forms.Button();
            this.eggsDisplayedLabel1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // enterLabel1
            // 
            this.enterLabel1.AutoSize = true;
            this.enterLabel1.Location = new System.Drawing.Point(253, 66);
            this.enterLabel1.Name = "enterLabel1";
            this.enterLabel1.Size = new System.Drawing.Size(214, 13);
            this.enterLabel1.TabIndex = 0;
            this.enterLabel1.Text = "Enter eggs produced by each of 5 chickens";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(256, 97);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(54, 20);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(316, 97);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(54, 20);
            this.textBox2.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(376, 97);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(54, 20);
            this.textBox3.TabIndex = 3;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(256, 123);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(54, 20);
            this.textBox4.TabIndex = 4;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(316, 123);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(54, 20);
            this.textBox5.TabIndex = 5;
            // 
            // calculateButton1
            // 
            this.calculateButton1.Location = new System.Drawing.Point(256, 171);
            this.calculateButton1.Name = "calculateButton1";
            this.calculateButton1.Size = new System.Drawing.Size(174, 39);
            this.calculateButton1.TabIndex = 6;
            this.calculateButton1.Text = "Click to calculate eggs in dozens";
            this.calculateButton1.UseVisualStyleBackColor = true;
            this.calculateButton1.Click += new System.EventHandler(this.calculateButton1_Click);
            // 
            // eggsDisplayedLabel1
            // 
            this.eggsDisplayedLabel1.AutoSize = true;
            this.eggsDisplayedLabel1.Location = new System.Drawing.Point(253, 231);
            this.eggsDisplayedLabel1.Name = "eggsDisplayedLabel1";
            this.eggsDisplayedLabel1.Size = new System.Drawing.Size(35, 13);
            this.eggsDisplayedLabel1.TabIndex = 7;
            this.eggsDisplayedLabel1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 384);
            this.Controls.Add(this.eggsDisplayedLabel1);
            this.Controls.Add(this.calculateButton1);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.enterLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label enterLabel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button calculateButton1;
        private System.Windows.Forms.Label eggsDisplayedLabel1;
    }
}

