﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PayrollGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double payRate;
            double hoursWorked;
            double grossPay;
            double fedWithholdTax;
            double stateWithholdTax;
            double netPay;
            double taxes;

            payRate = Convert.ToDouble(payRateTextBox.Text);
            hoursWorked = Convert.ToDouble(hoursWorkedTextBox.Text);
            grossPay = payRate * hoursWorked;
            fedWithholdTax = grossPay * .15;
            stateWithholdTax = grossPay * .05;
            taxes = fedWithholdTax + stateWithholdTax;
            netPay = grossPay - taxes;

            nameOutputLabel.Text = nameTextBox.Text;
            socialSecurityOutputLabel.Text = socialSecurityTextBox.Text;
            payRateOutputLabel.Text = payRateTextBox.Text;
            hoursWorkedOutputLabel.Text = hoursWorkedTextBox.Text;
            grossPayTotalLabel.Text = grossPay.ToString("C");
            federalTotalLabel.Text = fedWithholdTax.ToString("C");
            stateTotalLabel.Text = stateWithholdTax.ToString("C");
            netPayTotalLabel.Text = netPay.ToString("C");

        }
    }
}
