﻿namespace PayrollGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.socialSecurityLabel = new System.Windows.Forms.Label();
            this.payRateLabel = new System.Windows.Forms.Label();
            this.hoursWorkedLabel = new System.Windows.Forms.Label();
            this.socialSecurityTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.payRateTextBox = new System.Windows.Forms.TextBox();
            this.hoursWorkedTextBox = new System.Windows.Forms.TextBox();
            this.grossPayLabel = new System.Windows.Forms.Label();
            this.federalWithholdingTaxLabel = new System.Windows.Forms.Label();
            this.stateWithholdingTaxLabel = new System.Windows.Forms.Label();
            this.netPayLabel = new System.Windows.Forms.Label();
            this.grossPayTotalLabel = new System.Windows.Forms.Label();
            this.federalTotalLabel = new System.Windows.Forms.Label();
            this.stateTotalLabel = new System.Windows.Forms.Label();
            this.netPayTotalLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.hoursWorkedLabel2 = new System.Windows.Forms.Label();
            this.payRateLabel2 = new System.Windows.Forms.Label();
            this.socialSecurityLabel2 = new System.Windows.Forms.Label();
            this.nameLabel2 = new System.Windows.Forms.Label();
            this.hoursWorkedOutputLabel = new System.Windows.Forms.Label();
            this.payRateOutputLabel = new System.Windows.Forms.Label();
            this.socialSecurityOutputLabel = new System.Windows.Forms.Label();
            this.nameOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(12, 41);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(38, 13);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name:";
            // 
            // socialSecurityLabel
            // 
            this.socialSecurityLabel.AutoSize = true;
            this.socialSecurityLabel.Location = new System.Drawing.Point(12, 81);
            this.socialSecurityLabel.Name = "socialSecurityLabel";
            this.socialSecurityLabel.Size = new System.Drawing.Size(120, 13);
            this.socialSecurityLabel.TabIndex = 1;
            this.socialSecurityLabel.Text = "Social Security Number:";
            // 
            // payRateLabel
            // 
            this.payRateLabel.AutoSize = true;
            this.payRateLabel.Location = new System.Drawing.Point(12, 120);
            this.payRateLabel.Name = "payRateLabel";
            this.payRateLabel.Size = new System.Drawing.Size(87, 13);
            this.payRateLabel.TabIndex = 2;
            this.payRateLabel.Text = "Hourly Pay Rate:";
            // 
            // hoursWorkedLabel
            // 
            this.hoursWorkedLabel.AutoSize = true;
            this.hoursWorkedLabel.Location = new System.Drawing.Point(12, 154);
            this.hoursWorkedLabel.Name = "hoursWorkedLabel";
            this.hoursWorkedLabel.Size = new System.Drawing.Size(131, 13);
            this.hoursWorkedLabel.TabIndex = 3;
            this.hoursWorkedLabel.Text = "Number of Hours Worked:";
            // 
            // socialSecurityTextBox
            // 
            this.socialSecurityTextBox.Location = new System.Drawing.Point(197, 81);
            this.socialSecurityTextBox.Name = "socialSecurityTextBox";
            this.socialSecurityTextBox.Size = new System.Drawing.Size(100, 20);
            this.socialSecurityTextBox.TabIndex = 4;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(197, 41);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 5;
            // 
            // payRateTextBox
            // 
            this.payRateTextBox.Location = new System.Drawing.Point(197, 120);
            this.payRateTextBox.Name = "payRateTextBox";
            this.payRateTextBox.Size = new System.Drawing.Size(100, 20);
            this.payRateTextBox.TabIndex = 6;
            // 
            // hoursWorkedTextBox
            // 
            this.hoursWorkedTextBox.Location = new System.Drawing.Point(197, 154);
            this.hoursWorkedTextBox.Name = "hoursWorkedTextBox";
            this.hoursWorkedTextBox.Size = new System.Drawing.Size(100, 20);
            this.hoursWorkedTextBox.TabIndex = 7;
            // 
            // grossPayLabel
            // 
            this.grossPayLabel.AutoSize = true;
            this.grossPayLabel.Location = new System.Drawing.Point(479, 246);
            this.grossPayLabel.Name = "grossPayLabel";
            this.grossPayLabel.Size = new System.Drawing.Size(57, 13);
            this.grossPayLabel.TabIndex = 8;
            this.grossPayLabel.Text = "Gross pay:";
            // 
            // federalWithholdingTaxLabel
            // 
            this.federalWithholdingTaxLabel.AutoSize = true;
            this.federalWithholdingTaxLabel.Location = new System.Drawing.Point(479, 291);
            this.federalWithholdingTaxLabel.Name = "federalWithholdingTaxLabel";
            this.federalWithholdingTaxLabel.Size = new System.Drawing.Size(118, 13);
            this.federalWithholdingTaxLabel.TabIndex = 9;
            this.federalWithholdingTaxLabel.Text = "Federal withholding tax:";
            // 
            // stateWithholdingTaxLabel
            // 
            this.stateWithholdingTaxLabel.AutoSize = true;
            this.stateWithholdingTaxLabel.Location = new System.Drawing.Point(479, 338);
            this.stateWithholdingTaxLabel.Name = "stateWithholdingTaxLabel";
            this.stateWithholdingTaxLabel.Size = new System.Drawing.Size(108, 13);
            this.stateWithholdingTaxLabel.TabIndex = 10;
            this.stateWithholdingTaxLabel.Text = "State withholding tax:";
            // 
            // netPayLabel
            // 
            this.netPayLabel.AutoSize = true;
            this.netPayLabel.Location = new System.Drawing.Point(479, 379);
            this.netPayLabel.Name = "netPayLabel";
            this.netPayLabel.Size = new System.Drawing.Size(47, 13);
            this.netPayLabel.TabIndex = 11;
            this.netPayLabel.Text = "Net pay:";
            // 
            // grossPayTotalLabel
            // 
            this.grossPayTotalLabel.AutoSize = true;
            this.grossPayTotalLabel.Location = new System.Drawing.Point(646, 246);
            this.grossPayTotalLabel.Name = "grossPayTotalLabel";
            this.grossPayTotalLabel.Size = new System.Drawing.Size(35, 13);
            this.grossPayTotalLabel.TabIndex = 12;
            this.grossPayTotalLabel.Text = "label5";
            // 
            // federalTotalLabel
            // 
            this.federalTotalLabel.AutoSize = true;
            this.federalTotalLabel.Location = new System.Drawing.Point(646, 291);
            this.federalTotalLabel.Name = "federalTotalLabel";
            this.federalTotalLabel.Size = new System.Drawing.Size(35, 13);
            this.federalTotalLabel.TabIndex = 13;
            this.federalTotalLabel.Text = "label6";
            // 
            // stateTotalLabel
            // 
            this.stateTotalLabel.AutoSize = true;
            this.stateTotalLabel.Location = new System.Drawing.Point(646, 338);
            this.stateTotalLabel.Name = "stateTotalLabel";
            this.stateTotalLabel.Size = new System.Drawing.Size(35, 13);
            this.stateTotalLabel.TabIndex = 14;
            this.stateTotalLabel.Text = "label7";
            // 
            // netPayTotalLabel
            // 
            this.netPayTotalLabel.AutoSize = true;
            this.netPayTotalLabel.Location = new System.Drawing.Point(646, 379);
            this.netPayTotalLabel.Name = "netPayTotalLabel";
            this.netPayTotalLabel.Size = new System.Drawing.Size(35, 13);
            this.netPayTotalLabel.TabIndex = 15;
            this.netPayTotalLabel.Text = "label8";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(197, 194);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Click to calculate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // hoursWorkedLabel2
            // 
            this.hoursWorkedLabel2.AutoSize = true;
            this.hoursWorkedLabel2.Location = new System.Drawing.Point(12, 379);
            this.hoursWorkedLabel2.Name = "hoursWorkedLabel2";
            this.hoursWorkedLabel2.Size = new System.Drawing.Size(131, 13);
            this.hoursWorkedLabel2.TabIndex = 20;
            this.hoursWorkedLabel2.Text = "Number of Hours Worked:";
            // 
            // payRateLabel2
            // 
            this.payRateLabel2.AutoSize = true;
            this.payRateLabel2.Location = new System.Drawing.Point(12, 338);
            this.payRateLabel2.Name = "payRateLabel2";
            this.payRateLabel2.Size = new System.Drawing.Size(87, 13);
            this.payRateLabel2.TabIndex = 19;
            this.payRateLabel2.Text = "Hourly Pay Rate:";
            // 
            // socialSecurityLabel2
            // 
            this.socialSecurityLabel2.AutoSize = true;
            this.socialSecurityLabel2.Location = new System.Drawing.Point(12, 291);
            this.socialSecurityLabel2.Name = "socialSecurityLabel2";
            this.socialSecurityLabel2.Size = new System.Drawing.Size(120, 13);
            this.socialSecurityLabel2.TabIndex = 18;
            this.socialSecurityLabel2.Text = "Social Security Number:";
            // 
            // nameLabel2
            // 
            this.nameLabel2.AutoSize = true;
            this.nameLabel2.Location = new System.Drawing.Point(12, 246);
            this.nameLabel2.Name = "nameLabel2";
            this.nameLabel2.Size = new System.Drawing.Size(38, 13);
            this.nameLabel2.TabIndex = 17;
            this.nameLabel2.Text = "Name:";
            // 
            // hoursWorkedOutputLabel
            // 
            this.hoursWorkedOutputLabel.AutoSize = true;
            this.hoursWorkedOutputLabel.Location = new System.Drawing.Point(262, 379);
            this.hoursWorkedOutputLabel.Name = "hoursWorkedOutputLabel";
            this.hoursWorkedOutputLabel.Size = new System.Drawing.Size(35, 13);
            this.hoursWorkedOutputLabel.TabIndex = 24;
            this.hoursWorkedOutputLabel.Text = "label8";
            // 
            // payRateOutputLabel
            // 
            this.payRateOutputLabel.AutoSize = true;
            this.payRateOutputLabel.Location = new System.Drawing.Point(262, 338);
            this.payRateOutputLabel.Name = "payRateOutputLabel";
            this.payRateOutputLabel.Size = new System.Drawing.Size(35, 13);
            this.payRateOutputLabel.TabIndex = 23;
            this.payRateOutputLabel.Text = "label7";
            // 
            // socialSecurityOutputLabel
            // 
            this.socialSecurityOutputLabel.AutoSize = true;
            this.socialSecurityOutputLabel.Location = new System.Drawing.Point(262, 291);
            this.socialSecurityOutputLabel.Name = "socialSecurityOutputLabel";
            this.socialSecurityOutputLabel.Size = new System.Drawing.Size(35, 13);
            this.socialSecurityOutputLabel.TabIndex = 22;
            this.socialSecurityOutputLabel.Text = "label6";
            // 
            // nameOutputLabel
            // 
            this.nameOutputLabel.AutoSize = true;
            this.nameOutputLabel.Location = new System.Drawing.Point(262, 246);
            this.nameOutputLabel.Name = "nameOutputLabel";
            this.nameOutputLabel.Size = new System.Drawing.Size(35, 13);
            this.nameOutputLabel.TabIndex = 21;
            this.nameOutputLabel.Text = "label5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 423);
            this.Controls.Add(this.hoursWorkedOutputLabel);
            this.Controls.Add(this.payRateOutputLabel);
            this.Controls.Add(this.socialSecurityOutputLabel);
            this.Controls.Add(this.nameOutputLabel);
            this.Controls.Add(this.hoursWorkedLabel2);
            this.Controls.Add(this.payRateLabel2);
            this.Controls.Add(this.socialSecurityLabel2);
            this.Controls.Add(this.nameLabel2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.netPayTotalLabel);
            this.Controls.Add(this.stateTotalLabel);
            this.Controls.Add(this.federalTotalLabel);
            this.Controls.Add(this.grossPayTotalLabel);
            this.Controls.Add(this.netPayLabel);
            this.Controls.Add(this.stateWithholdingTaxLabel);
            this.Controls.Add(this.federalWithholdingTaxLabel);
            this.Controls.Add(this.grossPayLabel);
            this.Controls.Add(this.hoursWorkedTextBox);
            this.Controls.Add(this.payRateTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.socialSecurityTextBox);
            this.Controls.Add(this.hoursWorkedLabel);
            this.Controls.Add(this.payRateLabel);
            this.Controls.Add(this.socialSecurityLabel);
            this.Controls.Add(this.nameLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label socialSecurityLabel;
        private System.Windows.Forms.Label payRateLabel;
        private System.Windows.Forms.Label hoursWorkedLabel;
        private System.Windows.Forms.TextBox socialSecurityTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox payRateTextBox;
        private System.Windows.Forms.TextBox hoursWorkedTextBox;
        private System.Windows.Forms.Label grossPayLabel;
        private System.Windows.Forms.Label federalWithholdingTaxLabel;
        private System.Windows.Forms.Label stateWithholdingTaxLabel;
        private System.Windows.Forms.Label netPayLabel;
        private System.Windows.Forms.Label grossPayTotalLabel;
        private System.Windows.Forms.Label federalTotalLabel;
        private System.Windows.Forms.Label stateTotalLabel;
        private System.Windows.Forms.Label netPayTotalLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label hoursWorkedLabel2;
        private System.Windows.Forms.Label payRateLabel2;
        private System.Windows.Forms.Label socialSecurityLabel2;
        private System.Windows.Forms.Label nameLabel2;
        private System.Windows.Forms.Label hoursWorkedOutputLabel;
        private System.Windows.Forms.Label payRateOutputLabel;
        private System.Windows.Forms.Label socialSecurityOutputLabel;
        private System.Windows.Forms.Label nameOutputLabel;
    }
}

