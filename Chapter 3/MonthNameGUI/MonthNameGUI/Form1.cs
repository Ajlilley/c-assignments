﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonthNameGUI
{
    public partial class Form1 : Form
    {
        enum Month
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            int monthNumber;
            Month monthName;
            monthNumber = Convert.ToInt32(textBox1.Text);
            monthName = (Month)monthNumber;
            monthDisplayLabel.Text = Convert.ToString(monthName);
        }
    }
}
