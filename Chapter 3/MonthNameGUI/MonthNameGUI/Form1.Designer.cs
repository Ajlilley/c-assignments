﻿namespace MonthNameGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.integerLabel = new System.Windows.Forms.Label();
            this.monthLabel = new System.Windows.Forms.Label();
            this.monthDisplayLabel = new System.Windows.Forms.Label();
            this.convertButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // integerLabel
            // 
            this.integerLabel.AutoSize = true;
            this.integerLabel.Location = new System.Drawing.Point(55, 39);
            this.integerLabel.Name = "integerLabel";
            this.integerLabel.Size = new System.Drawing.Size(110, 13);
            this.integerLabel.TabIndex = 0;
            this.integerLabel.Text = "Input a month integer:";
            // 
            // monthLabel
            // 
            this.monthLabel.AutoSize = true;
            this.monthLabel.Location = new System.Drawing.Point(55, 123);
            this.monthLabel.Name = "monthLabel";
            this.monthLabel.Size = new System.Drawing.Size(131, 13);
            this.monthLabel.TabIndex = 1;
            this.monthLabel.Text = "The month you seleced is:";
            // 
            // monthDisplayLabel
            // 
            this.monthDisplayLabel.AutoSize = true;
            this.monthDisplayLabel.Location = new System.Drawing.Point(208, 123);
            this.monthDisplayLabel.Name = "monthDisplayLabel";
            this.monthDisplayLabel.Size = new System.Drawing.Size(35, 13);
            this.monthDisplayLabel.TabIndex = 2;
            this.monthDisplayLabel.Text = "label3";
            // 
            // convertButton
            // 
            this.convertButton.Location = new System.Drawing.Point(58, 82);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(107, 22);
            this.convertButton.TabIndex = 3;
            this.convertButton.Text = "Click to convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(211, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 304);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.monthDisplayLabel);
            this.Controls.Add(this.monthLabel);
            this.Controls.Add(this.integerLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label integerLabel;
        private System.Windows.Forms.Label monthLabel;
        private System.Windows.Forms.Label monthDisplayLabel;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.TextBox textBox1;
    }
}

