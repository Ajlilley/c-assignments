﻿namespace MilesToKilometersGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.milesLabel1 = new System.Windows.Forms.Label();
            this.kilometersLabel1 = new System.Windows.Forms.Label();
            this.milesTextBox1 = new System.Windows.Forms.TextBox();
            this.convertButton1 = new System.Windows.Forms.Button();
            this.kiloLabel1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // milesLabel1
            // 
            this.milesLabel1.AutoSize = true;
            this.milesLabel1.Location = new System.Drawing.Point(50, 68);
            this.milesLabel1.Name = "milesLabel1";
            this.milesLabel1.Size = new System.Drawing.Size(31, 13);
            this.milesLabel1.TabIndex = 0;
            this.milesLabel1.Text = "Miles";
            // 
            // kilometersLabel1
            // 
            this.kilometersLabel1.AutoSize = true;
            this.kilometersLabel1.Location = new System.Drawing.Point(50, 200);
            this.kilometersLabel1.Name = "kilometersLabel1";
            this.kilometersLabel1.Size = new System.Drawing.Size(55, 13);
            this.kilometersLabel1.TabIndex = 1;
            this.kilometersLabel1.Text = "Kilometers";
            // 
            // milesTextBox1
            // 
            this.milesTextBox1.Location = new System.Drawing.Point(175, 68);
            this.milesTextBox1.Name = "milesTextBox1";
            this.milesTextBox1.Size = new System.Drawing.Size(100, 20);
            this.milesTextBox1.TabIndex = 2;
            // 
            // convertButton1
            // 
            this.convertButton1.Location = new System.Drawing.Point(53, 126);
            this.convertButton1.Name = "convertButton1";
            this.convertButton1.Size = new System.Drawing.Size(222, 33);
            this.convertButton1.TabIndex = 4;
            this.convertButton1.Text = "Convert miles to kilometers";
            this.convertButton1.UseVisualStyleBackColor = true;
            this.convertButton1.Click += new System.EventHandler(this.convertButton1_Click);
            // 
            // kiloLabel1
            // 
            this.kiloLabel1.AutoSize = true;
            this.kiloLabel1.Location = new System.Drawing.Point(172, 200);
            this.kiloLabel1.Name = "kiloLabel1";
            this.kiloLabel1.Size = new System.Drawing.Size(35, 13);
            this.kiloLabel1.TabIndex = 5;
            this.kiloLabel1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 368);
            this.Controls.Add(this.kiloLabel1);
            this.Controls.Add(this.convertButton1);
            this.Controls.Add(this.milesTextBox1);
            this.Controls.Add(this.kilometersLabel1);
            this.Controls.Add(this.milesLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label milesLabel1;
        private System.Windows.Forms.Label kilometersLabel1;
        private System.Windows.Forms.TextBox milesTextBox1;
        private System.Windows.Forms.Button convertButton1;
        private System.Windows.Forms.Label kiloLabel1;
    }
}

