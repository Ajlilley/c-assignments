﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PigLatinGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string inputWord;
            inputWord = Convert.ToString(inputTextBox.Text);
            char firstLetter = inputWord[0];
            string pigLatin = inputWord.Substring(1) + firstLetter + "ay";
            wordInPigLatinLabel.Text = String.Format("The word {0} in Pig Laten is {1}", inputWord, pigLatin);
        }
    }
}
