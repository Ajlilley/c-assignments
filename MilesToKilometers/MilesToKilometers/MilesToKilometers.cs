﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilesToKilometers
{
    class MilesToKilometers
    {
        static void Main(string[] args)
        {
            const double miles = 1;
            const float kilometers = 1.6f;
            double MilesToKilo = miles * kilometers;
            Console.WriteLine("{0}" + " mile(s) is " + "{1}" + " kilometer(s)", miles, kilometers);
            Console.ReadLine();
        }
    }
}
