﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class Lyrics
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pull me close I feel the water reach around my neck");
            Console.WriteLine("Don't let go just show me that there's better days ahead");
            Console.WriteLine("Make me see how I'm only half the person I should be");
            Console.WriteLine("Pull me close set me free from this gravity");
            Console.ReadLine();
        }
    }
}
