﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3
{
    class Cooments
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Comments: nonexecuting statements that you add to document a program.");
            Console.WriteLine("");
            Console.WriteLine("Line Comments: start with two forward slashes ( // ) and continue to the end of the current line. Line comments can appear on a line by themselves or at the end of a line following execuable code.");
            Console.WriteLine("");
            Console.WriteLine("Block Comments: start with a foward slash and an asterisk ( /* ) and end with an asterisk and a forard slash ( */ ). Block comments can appear on a line by themselves, on a line before executable code, or after executable code. They can also extend across as many lines as needed.");
            Console.ReadLine();
        }
    }
}
