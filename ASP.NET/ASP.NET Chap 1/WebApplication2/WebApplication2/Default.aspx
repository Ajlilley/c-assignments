﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 1: Future Value</title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2{
            width: 172px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <img alt="Murach" class="style1" src="Images/MurachLogo.jpg" /><br />
            <h1>401K Future Value Calculator</h1>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">Monthly investment</td>
                    <td><asp:DropDownList ID="ddlMonthlyInvestment"
                        runat="server" Width="106px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="style3">Annual interest rate</td>
                    <td><asp:TextBox ID="txtInterestRate" runat="server"
                        Width="100px">3.0</asp:TextBox></td>
                </tr>
                <tr>
                    <td class="style3">Number of years</td>
                    <td><asp:TextBox ID="txtYears" runat="server"
                            Width="100px">10</asp:TextBox></td>
                </tr> 
                <tr>
                    <td class="style3">Future Value</td>
                    <td><asp:Label ID="lblFutureValue" runat="server"
                            Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td class="style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style3"><asp:Button ID="btnCalculate" runat="server"
                        Text="Calculate" Width="100px" OnClick="btnCalculate_Click" /></td>
                    <td><asp:Button ID="btnClear" runat="server"
                        Text="Clear" Width="100px" OnClick="btnClear_Click"
                        CausesValidation="False" /></td>
                </tr>
            </table>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                runat="server" ErrorMessage="Interest rate is required."
                ControlToValidate="txtInterestRate" Display="Dynamic"
                ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server"
                ErrorMessage="Interest rate must range from 1 to 20."
                ControlToValidate="txtInterestRate" Display="Dynamic"
                ForeColor="Red" Type="Double"
                MaximumValue="20" MinimumValue="1"></asp:RangeValidator>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                runat="server" ErrorMessage="Number of years is required."
                ControlToValidate="txtYears" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server"
                ErrorMessage="Years must range from 1 to 45."
                ControlToValidate="txtYears" Type="Integer" Display="Dynamic"
                ForeColor="Red" MaximumValue="45" MinimumValue="1">
            </asp:RangeValidator>
        </div>
    </form>
</body>
</html>

