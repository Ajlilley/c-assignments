﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        Random rand = new Random();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Image1.Visible = false;
            //Image2.Visible = true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //random button generator 1-4. If ran == 1 --> show rock picture for enemy.
            int randomNumber = rand.Next(1, 4);

            if (DropDownList1.Text == "Rock")
            {
                //Image1.Visible = true;
                Image1.ImageUrl = "..\\..\\Images\\Rock.jpg";
            }

            if (DropDownList1.Text == "Paper")
            {
                Image1.Visible = true;
                Image1.ImageUrl = "..\\..\\Images\\Paper.jpg";
            }

            if (DropDownList1.Text == "Scissors")
            {
                Image1.Visible = true;
                Image1.ImageUrl = "..\\..\\Images\\Scissors.jpg";
            }

            if (randomNumber == 1)
            {
                //Image2.Visible = true;
                Image2.ImageUrl = "..\\..\\Images\\Rock.jpg";
            }
            if (randomNumber == 2)
            {
                //Image2.Visible = true;
                Image2.ImageUrl = @"..\\..\\Images\\Paper.jpg";
            }
            if (randomNumber == 3)
            {
                //Image2.Visible = true;
                Image2.ImageUrl = @"..\\..\\Images\\Scissors.jpg";
            }

            if ((DropDownList1.Text == "Rock" && randomNumber == 1) || (DropDownList1.Text == "Paper" && randomNumber == 2) || (DropDownList1.Text == "Scissors" && randomNumber == 3))
            {
                //Image2.Visible = true;
                Winnerlabel.Text = "It's a tie!!";
            }

            if ((DropDownList1.Text == "Rock" && randomNumber == 2) || (DropDownList1.Text == "Paper" && randomNumber == 3) || (DropDownList1.Text == "Scissors" && randomNumber == 1))
            {
                Winnerlabel.Text = "You Lose!!";
            }
            if ((DropDownList1.Text == "Rock" && randomNumber == 3) || (DropDownList1.Text == "Paper" && randomNumber == 1) || (DropDownList1.Text == "Scissors" && randomNumber == 2))
            {
                Winnerlabel.Text = "You Win!!";
            }
        }
    }
}