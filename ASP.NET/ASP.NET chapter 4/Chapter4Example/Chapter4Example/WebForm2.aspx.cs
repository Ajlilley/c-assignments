﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Chapter4Example
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DropDownList1.DataBind();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataView table = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
            table.RowFilter = "ProductID = '" + DropDownList1.SelectedValue + "'";
            DataRowView row = table[0];

            lblProductId.Text = row["ProductID"].ToString();
            lblName.Text = row["Name"].ToString();
            lblShort.Text = row["ShortDescription"].ToString();
            lblLong.Text = row["LongDescription"].ToString();
            lblPrice.Text = Convert.ToDecimal(row["UnitPrice"]).ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm2.aspx");
        }
    }
}