﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Chapter4Example.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="ProductID" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [ProductID], [Name], [ShortDescription], [LongDescription], [UnitPrice] FROM [Products]"></asp:SqlDataSource>
    <br />
    <asp:Label ID="Label5" runat="server" Text="Product ID:"></asp:Label>
&nbsp;<asp:Label ID="lblProductId" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
&nbsp;<asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Short Description:"></asp:Label>
&nbsp;<asp:Label ID="lblShort" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:Label ID="Label3" runat="server" Text="Long Description:"></asp:Label>
&nbsp;<asp:Label ID="lblLong" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:Label ID="Label4" runat="server" Text="Unit Price:"></asp:Label>
&nbsp;<asp:Label ID="lblPrice" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
    <br />
</asp:Content>
