﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="TravelVoyage.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Fantastic Voyage Travel Agency"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Select a Destination"></asp:Label>
            <br />
            <br />
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>Select A Destination</asp:ListItem>
                <asp:ListItem Value="2050">Paris ($2050)</asp:ListItem>
                <asp:ListItem Value="3000">Italy ($3000)</asp:ListItem>
                <asp:ListItem Value="2600">Bahamas ($2600)</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Select a Package"></asp:Label>
            <br />
            <br />
            <asp:RadioButton ID="RadioButton1" GroupName="Package" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" Text="All inclusive Package" AutoPostBack="True" />
&nbsp;<asp:RadioButton ID="RadioButton2" GroupName="Package" runat="server" Text="Super Package" AutoPostBack="True" OnCheckedChanged="RadioButton2_CheckedChanged" />
&nbsp;<asp:RadioButton ID="RadioButton3" GroupName="Package" runat="server" Text="Mega-Gold Package" AutoPostBack="True" OnCheckedChanged="RadioButton3_CheckedChanged" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Select a Departure Date"></asp:Label>
            <br />
            <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged"></asp:Calendar>
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Select a Return Date"></asp:Label>
            <br />
            <asp:Calendar ID="Calendar2" runat="server" OnSelectionChanged="Calendar2_SelectionChanged"></asp:Calendar>
            <br />
            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
