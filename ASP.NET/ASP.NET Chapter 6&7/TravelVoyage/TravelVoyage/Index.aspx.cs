﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TravelVoyage
{
    public partial class Index : System.Web.UI.Page
    {
        Customer customer;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                customer = new Customer();
                Session["Customer"] = customer;
            }
            else
            {
                customer = (Customer)Session["Customer"];
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedIndex != 0)
                customer.DestinationPrice = Convert.ToDouble(DropDownList1.SelectedValue);
            CalcPrice();
        }

        public void CalcPrice()
        {
            customer.Total = customer.PackagePrice + customer.DestinationPrice;
            DisplayPrice();
        }

        public void DisplayPrice()
        {
            Label6.Text = "Package Price: " + customer.PackagePrice.ToString("C");
            Label7.Text = "Destination Price: " + customer.DestinationPrice.ToString("C");
            Label8.Text = "Total: " + customer.Total.ToString("C");
        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton1.Checked)
                customer.PackagePrice = 500.00;
            else
                customer.PackagePrice = 0.0;
            CalcPrice();
        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton2.Checked)
                customer.PackagePrice = 1500.00;
            else
                customer.PackagePrice = 0.0;
            CalcPrice();
        }

        protected void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton3.Checked)
                customer.PackagePrice = 2500.00;
            else
                customer.PackagePrice = 0.0;
            CalcPrice();
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            customer.DepartureDate = Calendar1.SelectedDate;
            Label9.Text = "Departure Date: " + customer.DepartureDate.ToLongDateString();
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            customer.ReturnDate = Calendar2.SelectedDate;
            Label10.Text = "Return Date " + customer.ReturnDate.ToLongDateString();
            DisplayDiscountMessage();
        }

        public void DisplayDiscountMessage()
        {
            Label11.Text = "Your discount will be credited back to your credit card " +
                " 15 days after your return " + (customer.ReturnDate.AddDays(15));
        }
    }
}