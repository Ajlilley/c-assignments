﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelVoyage
{
    public class Customer
    {
        public double DestinationPrice { get; set; }
        public double PackagePrice { get; set; }
        public double Total { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
    }
}