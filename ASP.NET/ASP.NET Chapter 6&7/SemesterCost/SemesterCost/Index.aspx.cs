﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SemesterCost
{
    public partial class Index : System.Web.UI.Page
    {
        Customer customer;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                customer = new Customer();
                Session["Customer"] = customer;
                //RadioButton1.Checked = true;
            }
            else
            {
                customer = (Customer)Session["Customer"];
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedIndex != 0)
            {
                customer.ProgramPrice = Convert.ToDouble(DropDownList1.SelectedValue);
                CalcPrice();
            }
            else
                DisplayProgramErrorMessage();
        }

        public void DisplayProgramErrorMessage()
        {
            programErrorLabel.Text = "You must select a program";
        }

        public void CalcPrice()
        {
            customer.Total = customer.ProgramPrice + customer.ToolkitPrice;
            //DisplayPrice();
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedIndex == 1)
            {
                customer.ToolkitPrice = Convert.ToDouble(5000.00);
            }
            else if (RadioButtonList1.SelectedIndex == 2)
            {
                customer.ToolkitPrice = Convert.ToDouble(400.00);
            }
            else if (RadioButtonList1.SelectedIndex == 3)
            {
                customer.ToolkitPrice = Convert.ToDouble(35.00);
            }
            else
                customer.ToolkitPrice = 0.0;
                CalcPrice();
        }

        public void DisplayToolkitMessage()
        {
           // toolkitErrorLabel.Text = "You must select a toolkit";
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           costLabel.Text = "Total: " + customer.Total.ToString("C");
        }
    }
}