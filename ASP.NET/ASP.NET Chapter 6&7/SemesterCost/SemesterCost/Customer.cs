﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemesterCost
{
    public class Customer
    {
        public double ProgramPrice { get; set; }
        public double ToolkitPrice { get; set; }
        public double Total { get; set; }
        public double DeliveryDate { get; set; }
    }
}