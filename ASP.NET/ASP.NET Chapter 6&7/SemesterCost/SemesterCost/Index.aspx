﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="SemesterCost.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="margin-left: 200px">
            <asp:Label ID="titleLabel" runat="server" Font-Bold="True" Font-Size="X-Large" TabIndex="3" Text="Spring 2018 Semester Cost Estimation"></asp:Label>
            </h1>
            <br />
            <br />
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>Select a Program</asp:ListItem>
                <asp:ListItem Value="5000">Automotive ($5000)</asp:ListItem>
                <asp:ListItem Value="6000">Electrical ($6000)</asp:ListItem>
                <asp:ListItem Value="7000">Information Technology($7000)</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="programErrorLabel" runat="server" ForeColor="Red" Text="You must select a program" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                <asp:ListItem>Please Select a Toolkit</asp:ListItem>
                <asp:ListItem Value="5000">Mac Tool Box($5000)</asp:ListItem>
                <asp:ListItem Value="400">Greenlee Electrician&#39;s Toolkit ($400)</asp:ListItem>
                <asp:ListItem Value="35">Belekin Computer Toolkit ($35)</asp:ListItem>
            </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
            ControlToValidate="RadioButtonList1" ErrorMessage="RequiredFieldValidator" ForeColor="Red">You must select a toolkit</asp:RequiredFieldValidator>
            <br />
            <br />
            <br />
            <asp:Label ID="orderDateLabel" runat="server" Text="OrderDate:"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True" TextMode="Date" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <asp:Label ID="orderDateErrorLabel" runat="server" ForeColor="Red" Text="You must select an order date" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="orderDateErrorLabel2" runat="server" ForeColor="Red" Text="Date must be within the next week" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="costLabel" runat="server" Text="Cost:"></asp:Label>
            <br />
            <asp:Label ID="deliveryDateeLabel" runat="server" Text="Delivery Date:"></asp:Label>
        </div>
    </form>
</body>
</html>
