﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertQuartsToLiters
{
    class Program
    {
        static void Main(string[] args)
        {
            double quarts;
            double liters;

            WriteLine("Enter in the number of quarts:");
            quarts = Convert.ToDouble(ReadLine());

            liters = DisplayConvertion(quarts);

            WriteLine(quarts + " quart(s) is " + liters + " liters.");
            ReadLine();
        }

        public static double DisplayConvertion(double convertion)
        {
            double liters = convertion * 0.966353;
            return liters;
        }
    }
}
