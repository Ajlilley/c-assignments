﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountVowelsModularized
{
    class Program
    {
        static void Main(string[] args)
        {
            string word;
            Write(" Enter a word or phrase >> ");
            word = ReadLine();
            CountVowels(word);
            ReadLine();
        }

        static void CountVowels(string word)
        {


            int vowel = 0;
            for (int x = 0; x < word.Length; x++)
            {
                string take = word.Substring(x, 1).ToLower();
                if (take == "a" || take == "e" || take == "i" || take == "o" || take == "u")
                    vowel++;
            }


            WriteLine(" There are {0} vowels in your word", vowel);

        }
    }
}
