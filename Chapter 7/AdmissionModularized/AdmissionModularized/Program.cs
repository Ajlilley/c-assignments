﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdmissionModularized
{
    class Program
    {
        static void Main(string[] args)
        {
            double gradeAverage;
            int testScore;
            Write("Enter a GPA >> ");
            gradeAverage = Convert.ToDouble(ReadLine());
            Write("Enter a test score >> ");
            testScore = Convert.ToInt32(ReadLine());
            Admission(gradeAverage, testScore);
            ReadLine();

        }

        static void Admission(double gradeAverage, int testScore)
        {
            string permission;

            if ((gradeAverage >= 3.0 && testScore >= 60) || (gradeAverage < 3.0 && testScore >= 80))
                permission = "Accepted!";
            else
                permission = "Denied!";
            WriteLine(permission);

        }
    }
}
