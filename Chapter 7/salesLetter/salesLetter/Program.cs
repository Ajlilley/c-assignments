﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace salesLetter
{
    class Program
    {
        static void Main (string[] args)
        {
            WriteLine("I am writing to apply for the programmer position advertised in Linkin.");
            WriteLine("The opportunity presented in this listing is very interesting, and I believe that my strong technical experience and education will make me a very competitive candidate for this position.");
            WriteLine("With an associate degree in Computer Programming, I have a basic understanding of software development.");
            WriteLine("I also have experience in learning and excelling at new technologies as needed.");
            WriteLine("You can contact me at either my:");
            DisplayContactInfo();
            WriteLine("");
            WriteLine("");
            WriteLine("Again my contact info is:");
            DisplayContactInfo();
            ReadLine();
        }

        private static void DisplayContactInfo()
        {
            WriteLine("land line mumber: 314-123-4566.");
            WriteLine("My cell number: 417-312-1234.");
            WriteLine("Or my email: Aaron_Lilley@insideranken.org.");

        }
    }
}
