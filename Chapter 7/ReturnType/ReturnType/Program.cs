﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReturnType
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(); is a string return type
            //String.Equals() is a string return type
            //String.Compare() is a string return type
            //Convert.ToInt32() is an Int return type
            //Convert.ToChar() is a Char return type
            //Array.Sort() is an Int return type
        }
    }
}
