﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexibleArrayMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numberOneArray = { 30, 40, 600 };
            int[] numberTwoArray = { 10, 177, 20, 55, 63, 41 };
            int[] numberThreeArray = { 11, 65, 21, 1, 415, 112, 62, 301, 199 };
            Flexible(numberOneArray, numberTwoArray, numberThreeArray);
            WriteLine();
            ReadLine();
        }

        static void Flexible(int[] numbers, int[] numbersTwo, int[] numbersThree)
        {
            int i;
            int sum = 0;
            Write("The numbers in the first array are");
            for (i = 0; i < numbers.Length; ++i)
            {
                Write(" " + numbers[i]);

            }
            WriteLine();

            sum = numbers.Sum();
            WriteLine("The sum of the first array is {0}", sum);


            Write("\n The numbers in the second array are");
            for (i = 0; i < numbersTwo.Length; ++i)
            {
                Write(" " + numbersTwo[i]);

            }
            sum = numbersTwo.Sum();
            WriteLine(" The sum of second array is {0}", sum);


            Write("\n The numbers in the third array are");
            for (i = 0; i < numbersThree.Length; ++i)
            {
                Write(" " + numbersThree[i]);

            }

            sum = numbersThree.Sum();
            Write(" The sum of third array is {0}", sum);

        }
    }
}
