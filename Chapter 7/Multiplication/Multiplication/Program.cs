﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiplication
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            Write("Enter an integer to multiply >> ");
            num = Convert.ToInt32(ReadLine());
            DisplayMultiplication(num);
            ReadLine();
        }
        static void DisplayMultiplication(int num)
        {

            for (int x = 2; x <= 10; x++)
                WriteLine("{0} X {1} = {2} ", x, num, x * num);

        }
    }
}
