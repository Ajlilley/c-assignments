﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintingEstimate
{
    class Program
    {
       static void Main(string[] args)
        {
            double length;
            double width;
            double price;
            //const double job = 6;

            WriteLine("Enter the length of the room in feet:");
            length = Convert.ToDouble(ReadLine());

            WriteLine("Enter the width of the room in feet:");
            width = Convert.ToDouble(ReadLine());

            price = DisplayPrice(length, width);

            WriteLine("The price to paint the whole room is " + price.ToString("C"));
            ReadLine();
        }

        public static double DisplayPrice(double length, double width)
        {
            double price = (length * width) * 6;
            return price;
        }
    }
}
