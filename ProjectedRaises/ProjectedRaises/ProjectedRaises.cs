﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectedRaises
{
    class ProjectedRaises
    {
        static void Main(string[] args)
        {
            double raise = 0.4;
            double sam = 40000;
            double emily = 50000;
            double dan = 45000;
            double SamNextYearSalary = sam * raise + sam;
            double EmilyNextYearSalary = emily * raise + emily;
            double DanNextYearSalary = dan * raise + dan;
            Console.WriteLine(string.Format("Sam's salary for next year will be " + "{0:C2}" ,SamNextYearSalary));
            Console.WriteLine(string.Format("Emily's salary for next year will be " + "{0:C2}", EmilyNextYearSalary));
            Console.WriteLine(string.Format("Dan's salary for next year will be " + "{0:C2}", DanNextYearSalary));
            Console.ReadLine();
        }
    }
}
