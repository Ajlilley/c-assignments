﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordsOfWisdom
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.MouseEnter += OnMouseEnterLabel1;
            label1.MouseLeave += OnMouseLeaveLabel1;
            label2.MouseEnter += OnMouseEnterLabel2;
            label2.MouseLeave += OnMouseLeaveLabel2;
            label3.MouseEnter += OnMouseEnterLabel3;
            label3.MouseLeave += OnMouseLeaveLabel3;
            label4.MouseEnter += OnMouseEnterLabel4;
            label4.MouseLeave += OnMouseLeaveLabel4;
        }

        private void OnMouseEnterLabel1(object sender, EventArgs e)
        {
            label1.BackColor = Color.GhostWhite;
        }
        private void OnMouseLeaveLabel1(object sender, EventArgs e)
        {
            label1.BackColor = Color.Black;
        }

        private void OnMouseEnterLabel2(object sender, EventArgs e)
        {
            label2.BackColor = Color.GhostWhite;
        }
        private void OnMouseLeaveLabel2(object sender, EventArgs e)
        {
            label2.BackColor = Color.Black;
        }

        private void OnMouseEnterLabel3(object sender, EventArgs e)
        {
            label3.BackColor = Color.GhostWhite;
        }
        private void OnMouseLeaveLabel3(object sender, EventArgs e)
        {
            label3.BackColor = Color.Black;
        }

        private void OnMouseEnterLabel4(object sender, EventArgs e)
        {
            label4.BackColor = Color.GhostWhite;
        }
        private void OnMouseLeaveLabel4(object sender, EventArgs e)
        {
            label4.BackColor = Color.Black;
        }
    }
}
