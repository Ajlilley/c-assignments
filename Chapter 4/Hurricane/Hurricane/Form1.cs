﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hurricane
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double category = Convert.ToDouble(windSpeedTextBox.Text);

            if (category >= 157)
                categoryLabel.Text = "The hurricane is a category 5 hurricane.";

            else
                if (category >= 130 && category < 157)
                categoryLabel.Text = "The hurricane is a category 4 hurricane.";

            else
                if (category >= 111 && category < 130)
                categoryLabel.Text = "The hurricane is a category 3 hurricane.";

            else
                if (category >= 96 && category < 111)
                categoryLabel.Text = "The hurricane is a category 2 hurricane.";

            else
                if (category >= 74 && category < 96)
                categoryLabel.Text = "The hurricane is a category 1 hurricane.";

            else
                if (category < 96)
                categoryLabel.Text = "The storm is not a hurricane.";
        }
    }
}
