﻿namespace CheckMonth2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.birthMonthOutputLabel = new System.Windows.Forms.Label();
            this.monthNumberTextBox = new System.Windows.Forms.TextBox();
            this.birthMonthLabel = new System.Windows.Forms.Label();
            this.dayLabel = new System.Windows.Forms.Label();
            this.dayTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(173, 184);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 37);
            this.button1.TabIndex = 7;
            this.button1.Text = "Calculate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // birthMonthOutputLabel
            // 
            this.birthMonthOutputLabel.AutoSize = true;
            this.birthMonthOutputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthMonthOutputLabel.Location = new System.Drawing.Point(110, 253);
            this.birthMonthOutputLabel.Name = "birthMonthOutputLabel";
            this.birthMonthOutputLabel.Size = new System.Drawing.Size(0, 20);
            this.birthMonthOutputLabel.TabIndex = 6;
            // 
            // monthNumberTextBox
            // 
            this.monthNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monthNumberTextBox.Location = new System.Drawing.Point(299, 48);
            this.monthNumberTextBox.Name = "monthNumberTextBox";
            this.monthNumberTextBox.Size = new System.Drawing.Size(124, 26);
            this.monthNumberTextBox.TabIndex = 5;
            // 
            // birthMonthLabel
            // 
            this.birthMonthLabel.AutoSize = true;
            this.birthMonthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthMonthLabel.Location = new System.Drawing.Point(63, 51);
            this.birthMonthLabel.Name = "birthMonthLabel";
            this.birthMonthLabel.Size = new System.Drawing.Size(203, 20);
            this.birthMonthLabel.TabIndex = 4;
            this.birthMonthLabel.Text = "Enter a number of a month:";
            // 
            // dayLabel
            // 
            this.dayLabel.AutoSize = true;
            this.dayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dayLabel.Location = new System.Drawing.Point(64, 121);
            this.dayLabel.Name = "dayLabel";
            this.dayLabel.Size = new System.Drawing.Size(110, 20);
            this.dayLabel.TabIndex = 8;
            this.dayLabel.Text = "Enter in a day:";
            // 
            // dayTextBox
            // 
            this.dayTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dayTextBox.Location = new System.Drawing.Point(299, 118);
            this.dayTextBox.Name = "dayTextBox";
            this.dayTextBox.Size = new System.Drawing.Size(124, 26);
            this.dayTextBox.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 382);
            this.Controls.Add(this.dayTextBox);
            this.Controls.Add(this.dayLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.birthMonthOutputLabel);
            this.Controls.Add(this.monthNumberTextBox);
            this.Controls.Add(this.birthMonthLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label birthMonthOutputLabel;
        private System.Windows.Forms.TextBox monthNumberTextBox;
        private System.Windows.Forms.Label birthMonthLabel;
        private System.Windows.Forms.Label dayLabel;
        private System.Windows.Forms.TextBox dayTextBox;
    }
}

