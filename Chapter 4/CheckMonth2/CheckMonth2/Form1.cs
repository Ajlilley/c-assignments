﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckMonth2
{
    public partial class Form1 : Form
    {
        enum Month
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int monthNumber;
            int dayNumber;
            Month monthName;
            monthNumber = Convert.ToInt32(monthNumberTextBox.Text);
            monthName = (Month)monthNumber;
            dayNumber = Convert.ToInt32(dayTextBox.Text);

            if(monthNumber == 1 && dayNumber >=1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 2 && dayNumber >= 1 && dayNumber <= 28)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 3 && dayNumber >= 1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 4 && dayNumber >= 1 && dayNumber <= 30)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 5 && dayNumber >= 1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 6 && dayNumber >= 1 && dayNumber <= 30)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 7 && dayNumber >= 1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 8 && dayNumber >= 1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 9 && dayNumber >= 1 && dayNumber <= 30)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 10 && dayNumber >= 1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 11 && dayNumber >= 1 && dayNumber <= 30)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else if(monthNumber == 12 && dayNumber >= 1 && dayNumber <= 31)
            {
                birthMonthOutputLabel.Text = "The month you selected (" + Convert.ToString(monthName) + ") and the day you selected (" + dayNumber + ") is a valid combination.";
            }
            else 
            {
                birthMonthOutputLabel.Text = ("An invaild month or day number was entered. Please try again.");
            }
        }
    }
}
