﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuessingGame
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            Random ranNumberGenerator = new Random();
            int randomNumber;
            int guess;
            randomNumber = ranNumberGenerator.Next(1, 11);
            guess = Convert.ToInt32(textBox1.Text);

            if(guess > randomNumber)
            {
                messageLabel.Text = "Sorry but your guess was too high. Try again.";
            }

            if(guess < randomNumber)
            {
                messageLabel.Text = "Sorry but your guess was too low. Try again.";
            }

            if (guess == randomNumber)
            {
                messageLabel.Text = "Correct! The random number was " + randomNumber;
            }
        }
    }
}
