﻿namespace Twitter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tweetLabel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.charactersLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tweetLabel
            // 
            this.tweetLabel.AutoSize = true;
            this.tweetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tweetLabel.Location = new System.Drawing.Point(52, 69);
            this.tweetLabel.Name = "tweetLabel";
            this.tweetLabel.Size = new System.Drawing.Size(52, 20);
            this.tweetLabel.TabIndex = 0;
            this.tweetLabel.Text = "Tweet";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(125, 71);
            this.textBox1.MaxLength = 140;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(420, 100);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // charactersLabel
            // 
            this.charactersLabel.AutoSize = true;
            this.charactersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.charactersLabel.Location = new System.Drawing.Point(122, 186);
            this.charactersLabel.Name = "charactersLabel";
            this.charactersLabel.Size = new System.Drawing.Size(174, 18);
            this.charactersLabel.TabIndex = 2;
            this.charactersLabel.Text = "140 characters remaining";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 328);
            this.Controls.Add(this.charactersLabel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tweetLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tweetLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label charactersLabel;
    }
}

