﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admissions
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double gradePointAverage = 3.0;
            double testScore = 60;
            double testScore1 = 80;
            double userInput1, userInput2;

            userInput1 = Convert.ToDouble(gpaTextBox.Text);
            userInput2 = Convert.ToDouble(testTextBox.Text);

            if (userInput1 >= gradePointAverage && userInput2 >= testScore)
            {
                messageLabel.Text = "You are accepted, congratulations!";
                return;
            }
            if (userInput1 < gradePointAverage && userInput2 >= testScore1)
            {
                messageLabel.Text = "You are accepted , conratulations!";
                return;
            }
            else
            {
                messageLabel.Text = " Sorry, but you do not meet requirements. ";
            }
        }
    }
}
