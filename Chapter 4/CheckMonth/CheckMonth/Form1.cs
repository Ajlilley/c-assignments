﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckMonth
{
    public partial class Form1 : Form
    {
        enum Month
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int monthNumber;
            Month monthName;
            monthNumber = Convert.ToInt32(monthNumberTextBox.Text);
            monthName = (Month)monthNumber;

            switch (monthNumber)
            {
                case 1:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 2:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 3:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 4:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 5:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 6:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 7:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 8:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 9:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 10:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 11:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                case 12:
                    birthMonthOutputLabel.Text = "The month you selected was " + Convert.ToString(monthName) + " because " + monthNumber + " is a valid month.";
                    break;
                default:
                    birthMonthOutputLabel.Text = ("An invaild month or day number was entered. Please try again.");
                    break;
            }
        }
    }
}
