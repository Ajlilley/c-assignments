﻿namespace CheckMonth
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.birthMonthLabel = new System.Windows.Forms.Label();
            this.monthNumberTextBox = new System.Windows.Forms.TextBox();
            this.birthMonthOutputLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // birthMonthLabel
            // 
            this.birthMonthLabel.AutoSize = true;
            this.birthMonthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthMonthLabel.Location = new System.Drawing.Point(12, 57);
            this.birthMonthLabel.Name = "birthMonthLabel";
            this.birthMonthLabel.Size = new System.Drawing.Size(203, 20);
            this.birthMonthLabel.TabIndex = 0;
            this.birthMonthLabel.Text = "Enter a number of a month:";
            // 
            // monthNumberTextBox
            // 
            this.monthNumberTextBox.Location = new System.Drawing.Point(248, 57);
            this.monthNumberTextBox.Name = "monthNumberTextBox";
            this.monthNumberTextBox.Size = new System.Drawing.Size(124, 20);
            this.monthNumberTextBox.TabIndex = 1;
            // 
            // birthMonthOutputLabel
            // 
            this.birthMonthOutputLabel.AutoSize = true;
            this.birthMonthOutputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthMonthOutputLabel.Location = new System.Drawing.Point(47, 133);
            this.birthMonthOutputLabel.Name = "birthMonthOutputLabel";
            this.birthMonthOutputLabel.Size = new System.Drawing.Size(0, 20);
            this.birthMonthOutputLabel.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(122, 190);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 37);
            this.button1.TabIndex = 3;
            this.button1.Text = "Calculate the month";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 370);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.birthMonthOutputLabel);
            this.Controls.Add(this.monthNumberTextBox);
            this.Controls.Add(this.birthMonthLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label birthMonthLabel;
        private System.Windows.Forms.TextBox monthNumberTextBox;
        private System.Windows.Forms.Label birthMonthOutputLabel;
        private System.Windows.Forms.Button button1;
    }
}

