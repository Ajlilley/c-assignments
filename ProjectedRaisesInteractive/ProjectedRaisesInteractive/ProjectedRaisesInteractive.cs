﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectedRaisesInteractive
{
    class ProjectedRaisesInteractive
    {
        static void Main(string[] args)
        {
            double raise = 0.4;
            int num;
            Console.WriteLine("Enter your salary: ");
            num = Convert.ToInt32(Console.ReadLine());
            double NextYearSalary = num * raise + num;
            Console.WriteLine("Your salary for next year will be " + "{0:C2}", NextYearSalary);
            Console.ReadLine();
        }
    }
}
