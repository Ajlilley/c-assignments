﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;

namespace ProductReceipt.App_Code
{
    public class Category
    {
        public string CategoryID { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
    }
}