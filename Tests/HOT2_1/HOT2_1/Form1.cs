﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT2_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double intensity = Convert.ToDouble(textBox1.Text);

            if(intensity <= 1.9 && intensity >= 1.0)
                messageLabel.Text = "Microearthquakes, not felt, or felt rarely. Recorded by seismographs.";
            else
                if (intensity <= 2.9 && intensity >= 2.0)
                messageLabel.Text = "Minor. Felt slightly by some people. No damage to buildings.";
            else
                if (intensity <= 3.9 && intensity >= 3.0)
                messageLabel.Text = "Minor. Often felt by people, but very rarely causes damage. Shaking of indoor objects can be noticeable.";
            else
                if (intensity <= 4.9 && intensity >= 4.0)
                messageLabel.Text = "Light. Noticeable shaking of indoor objects and rattling noises.";
            else
                if (intensity <= 5.9 && intensity >= 5.0)
                messageLabel.Text = "Moderate. Can cause damage of varying severity to poorly constructed buildings.";
            else
                if (intensity <= 6.9 && intensity >= 6.0)
                messageLabel.Text = "Strong. Damage to a moderate number of well-built structures in populated areas.";
            else
                if (intensity <= 7.9 && intensity >= 7.0)
                messageLabel.Text = "Major. Causes damage to most buildings, some to partially or completely collapse or receive severe damage.";
            else
                if (intensity <= 8.9 && intensity >= 8.0)
                messageLabel.Text = "Great. Major damage to buildings, structures likely to be destroyed.";
            else
                if (intensity >= 9.0)
                messageLabel.Text = "Major. At or near total destruction – severe damage or collapse to all buildings.";
        }
    }
}
