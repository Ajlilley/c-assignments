﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT2_3b
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            double length;
            double width;
            int timesPaid;
            int price1 = 25 * 20;
            int price2 = 35 * 20;
            int price3 = 50 * 20;
            timesPaid = Convert.ToInt32(timesPayTextBox.Text);
            length = Convert.ToDouble(lengthTextBox.Text);
            width = Convert.ToDouble(widthTextBox.Text);
            double squareFeet = length * width;

            if (squareFeet < 400 && timesPaid == 1)
            {
                weeklyFeePriceLabel.Text = "$25";
                totalFeePriceLlabel.Text = "$" + Convert.ToString(price1);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                if (squareFeet >= 400 && squareFeet < 600 && timesPaid == 1)
            {
                weeklyFeePriceLabel.Text = "$35";
                totalFeePriceLlabel.Text = "$" + Convert.ToString(price2);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                if (squareFeet >= 600 && timesPaid == 1)
            {
                weeklyFeePriceLabel.Text = "$50";
                totalFeePriceLlabel.Text = "$" + Convert.ToString(price3);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                 if (squareFeet < 400 && timesPaid == 2)
            {
                weeklyFeePriceLabel.Text = "$25 + $5 service charge per payment";
                totalFeePriceLlabel.Text = "$" + (2 * (500 / 2) + 10);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                if (squareFeet >= 400 && squareFeet < 600 && timesPaid == 2)
            {
                weeklyFeePriceLabel.Text = "$35 + $5 service charge per payment";
                totalFeePriceLlabel.Text = "$" + (2 * (700 / 2) + 10);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                if (squareFeet >= 600 && timesPaid == 2)
            {
                weeklyFeePriceLabel.Text = "$50 + $5 service charge per payment";
                totalFeePriceLlabel.Text = "$" + (2 * (1000 / 2) + 10);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                 if (squareFeet < 400 && timesPaid == 3)
            {
                weeklyFeePriceLabel.Text = "$25 + $3 weekly service fee";
                totalFeePriceLlabel.Text = "$" + (25 * 20 + 60);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                if (squareFeet >= 400 && squareFeet < 600 && timesPaid == 3)
            {
                weeklyFeePriceLabel.Text = "$35 + $3 service charge per payment";
                totalFeePriceLlabel.Text = "$" + (35 * 20 + 60);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
            else
                if (squareFeet >= 600 && timesPaid == 3)
            {
                weeklyFeePriceLabel.Text = "$50 + $3 service charge per payment";
                totalFeePriceLlabel.Text = "$" + (50 * 20 + 60);
                numPaymentLabel.Text = timesPayTextBox.Text;
            }
        }
    }
}
