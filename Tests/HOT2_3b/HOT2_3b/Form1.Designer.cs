﻿namespace HOT2_3b
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateButton = new System.Windows.Forms.Button();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.lengthTextBox = new System.Windows.Forms.TextBox();
            this.totalFeePriceLlabel = new System.Windows.Forms.Label();
            this.weeklyFeePriceLabel = new System.Windows.Forms.Label();
            this.totalFeeLabel = new System.Windows.Forms.Label();
            this.weeklyFeeLabel = new System.Windows.Forms.Label();
            this.widthLabel = new System.Windows.Forms.Label();
            this.lengthLabel = new System.Windows.Forms.Label();
            this.timesPayLabel = new System.Windows.Forms.Label();
            this.timesPayTextBox = new System.Windows.Forms.TextBox();
            this.numberOfPaymentsLabel = new System.Windows.Forms.Label();
            this.numPaymentLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculateButton.Location = new System.Drawing.Point(18, 232);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(153, 45);
            this.calculateButton.TabIndex = 17;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // widthTextBox
            // 
            this.widthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.widthTextBox.Location = new System.Drawing.Point(406, 75);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(131, 38);
            this.widthTextBox.TabIndex = 16;
            // 
            // lengthTextBox
            // 
            this.lengthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lengthTextBox.Location = new System.Drawing.Point(406, 6);
            this.lengthTextBox.Name = "lengthTextBox";
            this.lengthTextBox.Size = new System.Drawing.Size(131, 38);
            this.lengthTextBox.TabIndex = 15;
            // 
            // totalFeePriceLlabel
            // 
            this.totalFeePriceLlabel.AutoSize = true;
            this.totalFeePriceLlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalFeePriceLlabel.Location = new System.Drawing.Point(567, 359);
            this.totalFeePriceLlabel.Name = "totalFeePriceLlabel";
            this.totalFeePriceLlabel.Size = new System.Drawing.Size(86, 31);
            this.totalFeePriceLlabel.TabIndex = 14;
            this.totalFeePriceLlabel.Text = "label6";
            // 
            // weeklyFeePriceLabel
            // 
            this.weeklyFeePriceLabel.AutoSize = true;
            this.weeklyFeePriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weeklyFeePriceLabel.Location = new System.Drawing.Point(567, 299);
            this.weeklyFeePriceLabel.Name = "weeklyFeePriceLabel";
            this.weeklyFeePriceLabel.Size = new System.Drawing.Size(86, 31);
            this.weeklyFeePriceLabel.TabIndex = 13;
            this.weeklyFeePriceLabel.Text = "label7";
            // 
            // totalFeeLabel
            // 
            this.totalFeeLabel.AutoSize = true;
            this.totalFeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalFeeLabel.Location = new System.Drawing.Point(12, 359);
            this.totalFeeLabel.Name = "totalFeeLabel";
            this.totalFeeLabel.Size = new System.Drawing.Size(372, 31);
            this.totalFeeLabel.TabIndex = 12;
            this.totalFeeLabel.Text = "Total fee for 20-week season:";
            // 
            // weeklyFeeLabel
            // 
            this.weeklyFeeLabel.AutoSize = true;
            this.weeklyFeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weeklyFeeLabel.Location = new System.Drawing.Point(12, 299);
            this.weeklyFeeLabel.Name = "weeklyFeeLabel";
            this.weeklyFeeLabel.Size = new System.Drawing.Size(256, 31);
            this.weeklyFeeLabel.TabIndex = 11;
            this.weeklyFeeLabel.Text = "Weekly mowing fee:";
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.widthLabel.Location = new System.Drawing.Point(12, 78);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(302, 31);
            this.widthLabel.TabIndex = 10;
            this.widthLabel.Text = "Enter width of lawn in ft:";
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lengthLabel.Location = new System.Drawing.Point(12, 9);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(312, 31);
            this.lengthLabel.TabIndex = 9;
            this.lengthLabel.Text = "Enter length of lawn in ft:";
            // 
            // timesPayLabel
            // 
            this.timesPayLabel.AutoSize = true;
            this.timesPayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesPayLabel.Location = new System.Drawing.Point(12, 161);
            this.timesPayLabel.Name = "timesPayLabel";
            this.timesPayLabel.Size = new System.Drawing.Size(492, 31);
            this.timesPayLabel.TabIndex = 18;
            this.timesPayLabel.Text = "pay (1)  once, (2) twice, or (3) 20 times?";
            // 
            // timesPayTextBox
            // 
            this.timesPayTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesPayTextBox.Location = new System.Drawing.Point(573, 158);
            this.timesPayTextBox.Name = "timesPayTextBox";
            this.timesPayTextBox.Size = new System.Drawing.Size(141, 38);
            this.timesPayTextBox.TabIndex = 19;
            // 
            // numberOfPaymentsLabel
            // 
            this.numberOfPaymentsLabel.AutoSize = true;
            this.numberOfPaymentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPaymentsLabel.Location = new System.Drawing.Point(12, 424);
            this.numberOfPaymentsLabel.Name = "numberOfPaymentsLabel";
            this.numberOfPaymentsLabel.Size = new System.Drawing.Size(273, 31);
            this.numberOfPaymentsLabel.TabIndex = 20;
            this.numberOfPaymentsLabel.Text = "Number of payments:";
            // 
            // numPaymentLabel
            // 
            this.numPaymentLabel.AutoSize = true;
            this.numPaymentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numPaymentLabel.Location = new System.Drawing.Point(567, 424);
            this.numPaymentLabel.Name = "numPaymentLabel";
            this.numPaymentLabel.Size = new System.Drawing.Size(86, 31);
            this.numPaymentLabel.TabIndex = 21;
            this.numPaymentLabel.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 552);
            this.Controls.Add(this.numPaymentLabel);
            this.Controls.Add(this.numberOfPaymentsLabel);
            this.Controls.Add(this.timesPayTextBox);
            this.Controls.Add(this.timesPayLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.widthTextBox);
            this.Controls.Add(this.lengthTextBox);
            this.Controls.Add(this.totalFeePriceLlabel);
            this.Controls.Add(this.weeklyFeePriceLabel);
            this.Controls.Add(this.totalFeeLabel);
            this.Controls.Add(this.weeklyFeeLabel);
            this.Controls.Add(this.widthLabel);
            this.Controls.Add(this.lengthLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.TextBox lengthTextBox;
        private System.Windows.Forms.Label totalFeePriceLlabel;
        private System.Windows.Forms.Label weeklyFeePriceLabel;
        private System.Windows.Forms.Label totalFeeLabel;
        private System.Windows.Forms.Label weeklyFeeLabel;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.Label timesPayLabel;
        private System.Windows.Forms.TextBox timesPayTextBox;
        private System.Windows.Forms.Label numberOfPaymentsLabel;
        private System.Windows.Forms.Label numPaymentLabel;
    }
}

