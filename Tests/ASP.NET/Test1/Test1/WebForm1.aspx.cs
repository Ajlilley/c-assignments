﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Test1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label4.Visible = false;
            Image1.Visible = false;
        }

        protected void spinButton_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int randomNumber1 = rand.Next(1, 10);
            int randomNumber2 = rand.Next(1, 10);
            int randomNumber3 = rand.Next(1, 10);

            Label1.Text = randomNumber1.ToString() + "|";
            Label2.Text = randomNumber2.ToString() + "|";
            Label3.Text = randomNumber3.ToString() + "|";

            Image1.ImageUrl = "..\\..\\Image\\lucky coin.jpg";

            if ((randomNumber1 == 7) || (randomNumber2 == 7) || (randomNumber3 == 7))
            {
                Label4.Visible = true;
                Image1.Visible = true;
            }

            else
            {
                Label4.Visible = false;
                Image1.Visible = false;
            }

        }
    }
}