﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Invoice #"></asp:Label>
&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="InvoiceNumber" DataValueField="CustEmail" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            <br />
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [InvoiceNumber], [CustEmail], [OrderDate], [Subtotal], [Shipping], [SalesTax], [Total] FROM [Invoices]"></asp:SqlDataSource>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="Email Address:"></asp:Label>
&nbsp;<asp:Label ID="emailId" runat="server" Text="Label"></asp:Label>
            <br />
            <asp:Label ID="Label3" runat="server" Text="Date :"></asp:Label>
&nbsp;<asp:Label ID="dateId" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text="Sub Total:"></asp:Label>
            &nbsp;<asp:Label ID="subTotalId" runat="server" Text="Label"></asp:Label>
            <br />
            <asp:Label ID="Label5" runat="server" Text="Shipping:"></asp:Label>
            &nbsp;<asp:Label ID="shippingId" runat="server" Text="Label"></asp:Label>
            <br />
            <asp:Label ID="Label6" runat="server" Text="Tax:"></asp:Label>
            &nbsp;<asp:Label ID="taxId" runat="server" Text="Label"></asp:Label>
            <br />
            <asp:Label ID="Label7" runat="server" Text="Total:"></asp:Label>
            &nbsp;<asp:Label ID="totalId" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Contact Buyer" />
        </div>
    </form>
</body>
</html>
