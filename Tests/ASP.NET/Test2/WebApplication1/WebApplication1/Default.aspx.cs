﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DropDownList1.DataBind();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataView table = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
            table.RowFilter = "CustEmail = '" + DropDownList1.SelectedValue + "'";
            DataRowView row = table[0];

            emailId.Text = row["CustEmail"].ToString();
            dateId.Text = row["OrderDate"].ToString();
            subTotalId.Text = Convert.ToDecimal(row["SubTotal"]).ToString("C");
            shippingId.Text = Convert.ToDecimal(row["Shipping"]).ToString("C");
            taxId.Text = Convert.ToDecimal(row["SalesTax"]).ToString("C");
            totalId.Text = Convert.ToDecimal(row["Total"]).ToString("C");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name = emailId.Text.Trim();
            Session["name"] = name;
            Response.Redirect("page2.aspx");
        }
    }
}