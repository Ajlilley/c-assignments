﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan
{
    class Program
    {
        static void Main(string[] args)
        {
            BankLoan bankloan = new BankLoan();
            Console.WriteLine("What was the total bank loan amount?");
            bankloan.LoanAmount = Convert.ToDouble(Console.ReadLine());

            double num;
            if (!Double.TryParse(Convert.ToDouble(bankloan.LoanAmount, out num)))
            {
                Console.WriteLine("Not a valid number");
                Console.WriteLine("What was the total bank loan amount?");
            }
            else
            {
                Console.WriteLine("The original bank loan amount was {0:C}", bankloan.LoanAmount);
                Console.WriteLine("After 1 payment the bank loan is down to {0:C}", bankloan.SubtractPayment());
                Console.WriteLine("After 1 interest fee accumulation, the loan is now {0:C}", bankloan.NewLoan());
            }
            Console.ReadLine();
        }
    }
}
