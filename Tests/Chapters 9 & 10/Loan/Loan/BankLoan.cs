﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan
{
    class BankLoan
    {
        private double _loanAmount;
        const double interestRate = .05;
        int num = 200;
        double sub;
        public double LoanAmount
        {
            get
            {
                return this._loanAmount;
            }
            set
            {
                this._loanAmount = value;
            }
        }

        public double SubtractPayment()
       {
            sub = LoanAmount - num;

            return sub;
        }

        public double NewLoan()
        {
            double newPayment;
            newPayment = sub * interestRate;

            return newPayment;
        }
    }
}
