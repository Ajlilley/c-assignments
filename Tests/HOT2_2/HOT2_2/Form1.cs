﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT2_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double letterGrade;

            letterGrade = Convert.ToDouble(textBox1.Text);

            if(letterGrade <= 4.0 && letterGrade > 3.0)
                messageLabel.Text = "Your letter grade is an A!";
            else
                if (letterGrade <= 3.0 && letterGrade > 2.0)
                messageLabel.Text = "Your letter grade is a B!";
            else
                if (letterGrade <= 2.0 && letterGrade > 1.0)
                messageLabel.Text = "Your letter grade is a C!";
            else
                if (letterGrade <= 1.0 && letterGrade > 0.1)
                messageLabel.Text = "Your letter grade is a D!";
            else
                if (letterGrade == 0.0)
                messageLabel.Text = "Your letter grade is an F!";
        }
    }
}
