﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT2
{
    class HOT2
    {
        static void Main(string[] args)
        {
            double Number1;
            double Number2;
            double Number3;
            double Mean;
            Console.WriteLine("Enter in your first number:");
            Number1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Enter in your second number:");
            Number2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Enter in your third number:");
            Number3 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Mean = (Number1 + Number2 + Number3) / 3;
            Console.WriteLine("The Average of your three numbers is " + "{0}.", Mean);
            Console.ReadLine();
        }
    }
}
