﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT3
{
    class HOT3
    {
        static void Main(string[] args)
        {
            string Name;
            string Address;
            string City;
            string State;
            string Zip;
            double Shirts;
            double TaxPercent;
            double TaxTotal;
            const double ShirtPrice = 14.99;
            double AmountDue;
            double PriceTotal;
            Console.WriteLine("Enter your name:");
            Name = Console.ReadLine();
            Console.WriteLine("Enter your address:");
            Address = Console.ReadLine();
            Console.WriteLine("Enter your city:");
            City = Console.ReadLine();
            Console.WriteLine("Enter your state:");
            State = Console.ReadLine();
            Console.WriteLine("Enter your zip:");
            Zip = Console.ReadLine();
            Console.WriteLine("How many shirts do you want to order?");
            Shirts = Convert.ToInt32(Console.ReadLine());
            TaxPercent = 0.08;
            PriceTotal = ShirtPrice * Shirts;
            TaxTotal = PriceTotal * TaxPercent;
            AmountDue = (PriceTotal + TaxTotal);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Receipt for:");
            Console.WriteLine("{0}", Name);
            Console.WriteLine("{0}", Address);
            Console.WriteLine("{0}, {1} {2}", City, State, Zip);
            Console.WriteLine();
            Console.WriteLine("{0} " + "T-Shirts ordered @ $14.99 each", Shirts);
            Console.WriteLine();
            Console.WriteLine("Total:    " + "{0}", PriceTotal.ToString("C"));
            Console.WriteLine("Tax:      " + "{0}", TaxTotal.ToString("C"));
            Console.WriteLine("-------------------------");
            Console.WriteLine("Due:      " + "{0}", AmountDue.ToString("C"));
            Console.ReadLine();
        }
    }
}