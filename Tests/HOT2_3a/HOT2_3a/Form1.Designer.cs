﻿namespace HOT2_3a
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lengthLabel = new System.Windows.Forms.Label();
            this.widthLabel = new System.Windows.Forms.Label();
            this.weeklyFeeLabel = new System.Windows.Forms.Label();
            this.totalFeeLabel = new System.Windows.Forms.Label();
            this.weeklyFeePriceLabel = new System.Windows.Forms.Label();
            this.totalFeePriceLlabel = new System.Windows.Forms.Label();
            this.lengthTextBox = new System.Windows.Forms.TextBox();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lengthLabel.Location = new System.Drawing.Point(58, 47);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(312, 31);
            this.lengthLabel.TabIndex = 0;
            this.lengthLabel.Text = "Enter length of lawn in ft:";
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.widthLabel.Location = new System.Drawing.Point(58, 116);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(302, 31);
            this.widthLabel.TabIndex = 1;
            this.widthLabel.Text = "Enter width of lawn in ft:";
            // 
            // weeklyFeeLabel
            // 
            this.weeklyFeeLabel.AutoSize = true;
            this.weeklyFeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weeklyFeeLabel.Location = new System.Drawing.Point(58, 265);
            this.weeklyFeeLabel.Name = "weeklyFeeLabel";
            this.weeklyFeeLabel.Size = new System.Drawing.Size(256, 31);
            this.weeklyFeeLabel.TabIndex = 2;
            this.weeklyFeeLabel.Text = "Weekly mowing fee:";
            // 
            // totalFeeLabel
            // 
            this.totalFeeLabel.AutoSize = true;
            this.totalFeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalFeeLabel.Location = new System.Drawing.Point(58, 350);
            this.totalFeeLabel.Name = "totalFeeLabel";
            this.totalFeeLabel.Size = new System.Drawing.Size(372, 31);
            this.totalFeeLabel.TabIndex = 3;
            this.totalFeeLabel.Text = "Total fee for 20-week season:";
            // 
            // weeklyFeePriceLabel
            // 
            this.weeklyFeePriceLabel.AutoSize = true;
            this.weeklyFeePriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weeklyFeePriceLabel.Location = new System.Drawing.Point(446, 265);
            this.weeklyFeePriceLabel.Name = "weeklyFeePriceLabel";
            this.weeklyFeePriceLabel.Size = new System.Drawing.Size(0, 31);
            this.weeklyFeePriceLabel.TabIndex = 4;
            // 
            // totalFeePriceLlabel
            // 
            this.totalFeePriceLlabel.AutoSize = true;
            this.totalFeePriceLlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalFeePriceLlabel.Location = new System.Drawing.Point(446, 350);
            this.totalFeePriceLlabel.Name = "totalFeePriceLlabel";
            this.totalFeePriceLlabel.Size = new System.Drawing.Size(0, 31);
            this.totalFeePriceLlabel.TabIndex = 5;
            // 
            // lengthTextBox
            // 
            this.lengthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lengthTextBox.Location = new System.Drawing.Point(452, 44);
            this.lengthTextBox.Name = "lengthTextBox";
            this.lengthTextBox.Size = new System.Drawing.Size(131, 38);
            this.lengthTextBox.TabIndex = 6;
            // 
            // widthTextBox
            // 
            this.widthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.widthTextBox.Location = new System.Drawing.Point(452, 113);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(131, 38);
            this.widthTextBox.TabIndex = 7;
            // 
            // calculateButton
            // 
            this.calculateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculateButton.Location = new System.Drawing.Point(124, 187);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(153, 45);
            this.calculateButton.TabIndex = 8;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 445);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.widthTextBox);
            this.Controls.Add(this.lengthTextBox);
            this.Controls.Add(this.totalFeePriceLlabel);
            this.Controls.Add(this.weeklyFeePriceLabel);
            this.Controls.Add(this.totalFeeLabel);
            this.Controls.Add(this.weeklyFeeLabel);
            this.Controls.Add(this.widthLabel);
            this.Controls.Add(this.lengthLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label weeklyFeeLabel;
        private System.Windows.Forms.Label totalFeeLabel;
        private System.Windows.Forms.Label weeklyFeePriceLabel;
        private System.Windows.Forms.Label totalFeePriceLlabel;
        private System.Windows.Forms.TextBox lengthTextBox;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.Button calculateButton;
    }
}

