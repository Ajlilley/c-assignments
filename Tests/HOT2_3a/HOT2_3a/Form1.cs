﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT2_3a
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            double length;
            double width;
            int price1 = 25 * 20;
            int price2 = 35 * 20;
            int price3 = 50 * 20;
            length = Convert.ToDouble(lengthTextBox.Text);
            width = Convert.ToDouble(widthTextBox.Text);
            double squareFeet = length * width;

            if (squareFeet < 400)
            {
                weeklyFeePriceLabel.Text = "$25";
                totalFeePriceLlabel.Text = "$" + Convert.ToString(price1);
            }
            else
                if (squareFeet >= 400 && squareFeet < 600)
            {
                weeklyFeePriceLabel.Text = "$35";
                totalFeePriceLlabel.Text = "$" + Convert.ToString(price2);
            }
            else
                if (squareFeet >= 600)
            {
                weeklyFeePriceLabel.Text = "$50";
                totalFeePriceLlabel.Text = "$" + Convert.ToString(price3);
            }
        }
    }
}
