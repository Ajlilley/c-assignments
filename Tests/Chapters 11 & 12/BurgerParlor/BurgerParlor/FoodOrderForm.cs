﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SundaeParlor
{
    public partial class Form1 : Form
    {
        public double _totalPrice = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void SundaeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sundaeCheckBox.Checked)
            {
                sprinklesCheckBox.Enabled = true;
                nutsCheckBox.Enabled = true;
                chocolateCheckBox.Enabled = true;
                sprinklesCheckBox.BackColor = Color.LightBlue;
                nutsCheckBox.BackColor = Color.LightBlue;
                chocolateCheckBox.BackColor = Color.LightBlue;
            }

            else
            {
                sprinklesCheckBox.Enabled = false;
                nutsCheckBox.Enabled = false;
                chocolateCheckBox.Enabled = false;
                sprinklesCheckBox.Checked = false;
                nutsCheckBox.Checked = false;
                chocolateCheckBox.Checked = false;
                sprinklesCheckBox.BackColor = Color.Transparent;
                nutsCheckBox.BackColor = Color.Transparent;
                chocolateCheckBox.BackColor = Color.Transparent;
            }
        }

        private void sodaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sodaCheckBox.Checked)
            {
                limeCheckBox.Enabled = true;
                peachCheckBox.Enabled = true;
                mangoCheckBox.Enabled = true;
                limeCheckBox.BackColor = Color.LightBlue;
                peachCheckBox.BackColor = Color.LightBlue;
                mangoCheckBox.BackColor = Color.LightBlue;
            }

            else
            {
                limeCheckBox.Enabled = false;
                peachCheckBox.Enabled = false;
                mangoCheckBox.Enabled = false;
                limeCheckBox.Checked = false;
                peachCheckBox.Checked = false;
                mangoCheckBox.Checked = false;
                limeCheckBox.BackColor = Color.Transparent;
                peachCheckBox.BackColor = Color.Transparent;
                mangoCheckBox.BackColor = Color.Transparent;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
               sprinklesCheckBox.Enabled = false;
               nutsCheckBox.Enabled = false;
               chocolateCheckBox.Enabled = false;
               limeCheckBox.Enabled = false;
               peachCheckBox.Enabled = false;
               mangoCheckBox.Enabled = false;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Order order;

            string name = nameTextBox.Text;

            bool hasSundae = false, hasSoda = false;

            if(sundaeCheckBox.Checked)
            {
                hasSundae = true;
            }
            if(sodaCheckBox.Checked)
            {
                hasSoda = true;
            }

            try
            {
                order = new Order(name, hasSundae, hasSoda);
                nameErrorLabel.Text = "";
            }
            catch (NameMissing ex)
            {
                nameErrorLabel.Text = ex.Message;
                return;
            }
            catch (NoFood ex)
            {
                nameErrorLabel.Text = ex.Message;
                return;
            }

            try
            {
                if (sprinklesCheckBox.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.SPRINKLES);
                }
                if (nutsCheckBox.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.NUTS);
                }
                if (chocolateCheckBox.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.CHOCOLATE_SYRUP);
                }
                sundaeErrorLabel.Text = "";
            }
            catch(TooManyToppings ex)
            {
                sundaeErrorLabel.Text = ex.Message;
                return;
            }

            try
            {
                if (mangoCheckBox.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.MANGO);
                }

                if (limeCheckBox.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.LIME);
                }

                if (peachCheckBox.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.PEACH);
                }
                sodaErrorLabel.Text = "";
            }
            catch(TooManyFlavors ex)
            {
                sodaErrorLabel.Text = ex.Message;
                return;
            }

            orderTextBox.Text += order.ToString();
            _totalPrice += order.Price;
            priceLabel.Text = String.Format("Total: {0:C}", _totalPrice);
        }
    }
}
