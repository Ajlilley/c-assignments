﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundaeParlor
{
    public class TooManyFlavors : ApplicationException
    {
        public TooManyFlavors() : base("Only 1 mixin allowed")
        {

        }
    }
}
