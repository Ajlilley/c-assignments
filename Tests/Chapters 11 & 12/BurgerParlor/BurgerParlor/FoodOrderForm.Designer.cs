﻿namespace SundaeParlor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.nameErrorLabel = new System.Windows.Forms.Label();
            this.sundaeLabel = new System.Windows.Forms.Label();
            this.sodaLabel = new System.Windows.Forms.Label();
            this.sundaeToppingsLabel = new System.Windows.Forms.Label();
            this.sodaMixinsLabel = new System.Windows.Forms.Label();
            this.orderLabel = new System.Windows.Forms.Label();
            this.sundaeErrorLabel = new System.Windows.Forms.Label();
            this.sodaErrorLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.sundaeCheckBox = new System.Windows.Forms.CheckBox();
            this.sodaCheckBox = new System.Windows.Forms.CheckBox();
            this.sprinklesCheckBox = new System.Windows.Forms.CheckBox();
            this.nutsCheckBox = new System.Windows.Forms.CheckBox();
            this.chocolateCheckBox = new System.Windows.Forms.CheckBox();
            this.limeCheckBox = new System.Windows.Forms.CheckBox();
            this.peachCheckBox = new System.Windows.Forms.CheckBox();
            this.mangoCheckBox = new System.Windows.Forms.CheckBox();
            this.orderTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(12, 45);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(295, 18);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "What name do you want on the order?";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(15, 75);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(292, 20);
            this.nameTextBox.TabIndex = 1;
            this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
            // 
            // nameErrorLabel
            // 
            this.nameErrorLabel.AutoSize = true;
            this.nameErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.nameErrorLabel.Location = new System.Drawing.Point(12, 98);
            this.nameErrorLabel.Name = "nameErrorLabel";
            this.nameErrorLabel.Size = new System.Drawing.Size(0, 15);
            this.nameErrorLabel.TabIndex = 2;
            // 
            // sundaeLabel
            // 
            this.sundaeLabel.AutoSize = true;
            this.sundaeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sundaeLabel.Location = new System.Drawing.Point(12, 152);
            this.sundaeLabel.Name = "sundaeLabel";
            this.sundaeLabel.Size = new System.Drawing.Size(168, 16);
            this.sundaeLabel.TabIndex = 3;
            this.sundaeLabel.Text = "Do you want a sundae?";
            // 
            // sodaLabel
            // 
            this.sodaLabel.AutoSize = true;
            this.sodaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sodaLabel.Location = new System.Drawing.Point(237, 152);
            this.sodaLabel.Name = "sodaLabel";
            this.sodaLabel.Size = new System.Drawing.Size(152, 16);
            this.sodaLabel.TabIndex = 4;
            this.sodaLabel.Text = "Do you want a soda?";
            // 
            // sundaeToppingsLabel
            // 
            this.sundaeToppingsLabel.AutoSize = true;
            this.sundaeToppingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sundaeToppingsLabel.Location = new System.Drawing.Point(12, 239);
            this.sundaeToppingsLabel.Name = "sundaeToppingsLabel";
            this.sundaeToppingsLabel.Size = new System.Drawing.Size(138, 18);
            this.sundaeToppingsLabel.TabIndex = 5;
            this.sundaeToppingsLabel.Text = "Sundae Toppings";
            // 
            // sodaMixinsLabel
            // 
            this.sodaMixinsLabel.AutoSize = true;
            this.sodaMixinsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sodaMixinsLabel.Location = new System.Drawing.Point(237, 239);
            this.sodaMixinsLabel.Name = "sodaMixinsLabel";
            this.sodaMixinsLabel.Size = new System.Drawing.Size(101, 18);
            this.sodaMixinsLabel.TabIndex = 6;
            this.sodaMixinsLabel.Text = "Drink Mixins";
            // 
            // orderLabel
            // 
            this.orderLabel.AutoSize = true;
            this.orderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderLabel.Location = new System.Drawing.Point(689, 46);
            this.orderLabel.Name = "orderLabel";
            this.orderLabel.Size = new System.Drawing.Size(64, 24);
            this.orderLabel.TabIndex = 7;
            this.orderLabel.Text = "Order";
            // 
            // sundaeErrorLabel
            // 
            this.sundaeErrorLabel.AutoSize = true;
            this.sundaeErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.sundaeErrorLabel.Location = new System.Drawing.Point(12, 352);
            this.sundaeErrorLabel.Name = "sundaeErrorLabel";
            this.sundaeErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.sundaeErrorLabel.TabIndex = 8;
            // 
            // sodaErrorLabel
            // 
            this.sodaErrorLabel.AutoSize = true;
            this.sodaErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.sodaErrorLabel.Location = new System.Drawing.Point(235, 352);
            this.sodaErrorLabel.Name = "sodaErrorLabel";
            this.sodaErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.sodaErrorLabel.TabIndex = 9;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(621, 377);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(62, 24);
            this.priceLabel.TabIndex = 10;
            this.priceLabel.Text = "Total:";
            // 
            // sundaeCheckBox
            // 
            this.sundaeCheckBox.AutoSize = true;
            this.sundaeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sundaeCheckBox.Location = new System.Drawing.Point(15, 179);
            this.sundaeCheckBox.Name = "sundaeCheckBox";
            this.sundaeCheckBox.Size = new System.Drawing.Size(46, 19);
            this.sundaeCheckBox.TabIndex = 11;
            this.sundaeCheckBox.Text = "Yes";
            this.sundaeCheckBox.UseVisualStyleBackColor = true;
            this.sundaeCheckBox.CheckedChanged += new System.EventHandler(this.SundaeCheckBox_CheckedChanged);
            // 
            // sodaCheckBox
            // 
            this.sodaCheckBox.AutoSize = true;
            this.sodaCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sodaCheckBox.Location = new System.Drawing.Point(240, 179);
            this.sodaCheckBox.Name = "sodaCheckBox";
            this.sodaCheckBox.Size = new System.Drawing.Size(46, 19);
            this.sodaCheckBox.TabIndex = 12;
            this.sodaCheckBox.Text = "Yes";
            this.sodaCheckBox.UseVisualStyleBackColor = true;
            this.sodaCheckBox.CheckedChanged += new System.EventHandler(this.sodaCheckBox_CheckedChanged);
            // 
            // sprinklesCheckBox
            // 
            this.sprinklesCheckBox.AutoSize = true;
            this.sprinklesCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this.sprinklesCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sprinklesCheckBox.Location = new System.Drawing.Point(15, 266);
            this.sprinklesCheckBox.Name = "sprinklesCheckBox";
            this.sprinklesCheckBox.Size = new System.Drawing.Size(77, 19);
            this.sprinklesCheckBox.TabIndex = 13;
            this.sprinklesCheckBox.Text = "Sprinkles";
            this.sprinklesCheckBox.UseVisualStyleBackColor = false;
            // 
            // nutsCheckBox
            // 
            this.nutsCheckBox.AutoSize = true;
            this.nutsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nutsCheckBox.Location = new System.Drawing.Point(15, 289);
            this.nutsCheckBox.Name = "nutsCheckBox";
            this.nutsCheckBox.Size = new System.Drawing.Size(51, 19);
            this.nutsCheckBox.TabIndex = 14;
            this.nutsCheckBox.Text = "Nuts";
            this.nutsCheckBox.UseVisualStyleBackColor = true;
            // 
            // chocolateCheckBox
            // 
            this.chocolateCheckBox.AutoSize = true;
            this.chocolateCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chocolateCheckBox.Location = new System.Drawing.Point(15, 312);
            this.chocolateCheckBox.Name = "chocolateCheckBox";
            this.chocolateCheckBox.Size = new System.Drawing.Size(115, 19);
            this.chocolateCheckBox.TabIndex = 15;
            this.chocolateCheckBox.Text = "Chocolate Syrup";
            this.chocolateCheckBox.UseVisualStyleBackColor = true;
            // 
            // limeCheckBox
            // 
            this.limeCheckBox.AutoSize = true;
            this.limeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.limeCheckBox.Location = new System.Drawing.Point(240, 266);
            this.limeCheckBox.Name = "limeCheckBox";
            this.limeCheckBox.Size = new System.Drawing.Size(90, 19);
            this.limeCheckBox.TabIndex = 16;
            this.limeCheckBox.Text = "Lime Flavor";
            this.limeCheckBox.UseVisualStyleBackColor = true;
            // 
            // peachCheckBox
            // 
            this.peachCheckBox.AutoSize = true;
            this.peachCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.peachCheckBox.Location = new System.Drawing.Point(240, 289);
            this.peachCheckBox.Name = "peachCheckBox";
            this.peachCheckBox.Size = new System.Drawing.Size(97, 19);
            this.peachCheckBox.TabIndex = 17;
            this.peachCheckBox.Text = "Peach Flavor";
            this.peachCheckBox.UseVisualStyleBackColor = true;
            // 
            // mangoCheckBox
            // 
            this.mangoCheckBox.AutoSize = true;
            this.mangoCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mangoCheckBox.Location = new System.Drawing.Point(240, 312);
            this.mangoCheckBox.Name = "mangoCheckBox";
            this.mangoCheckBox.Size = new System.Drawing.Size(101, 19);
            this.mangoCheckBox.TabIndex = 18;
            this.mangoCheckBox.Text = "Mango Flavor";
            this.mangoCheckBox.UseVisualStyleBackColor = true;
            // 
            // orderTextBox
            // 
            this.orderTextBox.Location = new System.Drawing.Point(404, 73);
            this.orderTextBox.Multiline = true;
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.orderTextBox.Size = new System.Drawing.Size(349, 290);
            this.orderTextBox.TabIndex = 19;
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(12, 393);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(159, 32);
            this.addButton.TabIndex = 20;
            this.addButton.Text = "Add item to order";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 2, 750, 2);
            this.label1.Size = new System.Drawing.Size(837, 24);
            this.label1.TabIndex = 21;
            this.label1.Text = "Food Order";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 456);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 2, 835, 2);
            this.label2.Size = new System.Drawing.Size(837, 24);
            this.label2.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 480);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.orderTextBox);
            this.Controls.Add(this.mangoCheckBox);
            this.Controls.Add(this.peachCheckBox);
            this.Controls.Add(this.limeCheckBox);
            this.Controls.Add(this.chocolateCheckBox);
            this.Controls.Add(this.nutsCheckBox);
            this.Controls.Add(this.sprinklesCheckBox);
            this.Controls.Add(this.sodaCheckBox);
            this.Controls.Add(this.sundaeCheckBox);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.sodaErrorLabel);
            this.Controls.Add(this.sundaeErrorLabel);
            this.Controls.Add(this.orderLabel);
            this.Controls.Add(this.sodaMixinsLabel);
            this.Controls.Add(this.sundaeToppingsLabel);
            this.Controls.Add(this.sodaLabel);
            this.Controls.Add(this.sundaeLabel);
            this.Controls.Add(this.nameErrorLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.nameLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label nameErrorLabel;
        private System.Windows.Forms.Label sundaeLabel;
        private System.Windows.Forms.Label sodaLabel;
        private System.Windows.Forms.Label sundaeToppingsLabel;
        private System.Windows.Forms.Label sodaMixinsLabel;
        private System.Windows.Forms.Label orderLabel;
        private System.Windows.Forms.Label sundaeErrorLabel;
        private System.Windows.Forms.Label sodaErrorLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.CheckBox sundaeCheckBox;
        private System.Windows.Forms.CheckBox sodaCheckBox;
        private System.Windows.Forms.CheckBox sprinklesCheckBox;
        private System.Windows.Forms.CheckBox nutsCheckBox;
        private System.Windows.Forms.CheckBox chocolateCheckBox;
        private System.Windows.Forms.CheckBox limeCheckBox;
        private System.Windows.Forms.CheckBox peachCheckBox;
        private System.Windows.Forms.CheckBox mangoCheckBox;
        private System.Windows.Forms.TextBox orderTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

