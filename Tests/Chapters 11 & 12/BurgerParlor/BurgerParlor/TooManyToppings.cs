﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundaeParlor
{
    public class TooManyToppings : ApplicationException
    {
        public TooManyToppings() : base("Only 2 toppings allowed")
        {

        }
    }
}
