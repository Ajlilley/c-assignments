﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundaeParlor
{
    public class NoFood : ApplicationException
    {
        public NoFood() : base("No food selected")
        {

        }
    }
}
