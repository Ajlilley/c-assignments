﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundaeParlor
{
    class NameMissing : ApplicationException
    {
        public NameMissing() : base("Name Required")
        {

        }
    }
}
