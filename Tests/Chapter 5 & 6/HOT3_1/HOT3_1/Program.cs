﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Enter a integer, 2-10 >> ");
            int number = Convert.ToInt32(ReadLine());

            for (int x = 1; x < number; x++)
                WriteLine("{0} X {1} = {2} ", x, number, x * number);

            ReadLine();
        }
    }
}
