﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT_3_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void totalButton_Click(object sender, EventArgs e)
        {
            int[] numbers = new int[] { 0, 5, 8, 9, 22, 45 };
            int sum = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                sum += numbers[i];
                totalTextBox.Text = Convert.ToString(sum);
            }
        }

        private void avgButton_Click(object sender, EventArgs e)
        {
            int[] numbers = new int[] { 0, 5, 8, 9, 22, 45 };
            double sum = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                sum += numbers[i];
                double avg = sum / numbers.Length;
                avgTextBox.Text = avg.ToString("f2");
            }
        }

        private void highestButton_Click(object sender, EventArgs e)
        {
            int[] numbers = new int[] { 0, 5, 8, 9, 22, 45 };
            int max = numbers.Max();

            highestTextBox.Text = Convert.ToString(max);
        }

        private void lowestButton_Click(object sender, EventArgs e)
        {
            int[] numbers = new int[] { 0, 5, 8, 9, 22, 45 };
            int min = numbers.Min();

            lowestTextBox.Text = Convert.ToString(min);
        }
    }
}
