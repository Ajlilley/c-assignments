﻿namespace HOT_3_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalLabel = new System.Windows.Forms.Label();
            this.totalTextBox = new System.Windows.Forms.TextBox();
            this.totalButton = new System.Windows.Forms.Button();
            this.avgButton = new System.Windows.Forms.Button();
            this.avgLabel = new System.Windows.Forms.Label();
            this.highestLabel = new System.Windows.Forms.Label();
            this.lowestLabel = new System.Windows.Forms.Label();
            this.avgTextBox = new System.Windows.Forms.TextBox();
            this.highestTextBox = new System.Windows.Forms.TextBox();
            this.lowestTextBox = new System.Windows.Forms.TextBox();
            this.highestButton = new System.Windows.Forms.Button();
            this.lowestButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.Location = new System.Drawing.Point(12, 165);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(83, 31);
            this.totalLabel.TabIndex = 2;
            this.totalLabel.Text = "Total:";
            // 
            // totalTextBox
            // 
            this.totalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTextBox.Location = new System.Drawing.Point(104, 162);
            this.totalTextBox.Name = "totalTextBox";
            this.totalTextBox.Size = new System.Drawing.Size(192, 38);
            this.totalTextBox.TabIndex = 4;
            // 
            // totalButton
            // 
            this.totalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalButton.Location = new System.Drawing.Point(104, 100);
            this.totalButton.Name = "totalButton";
            this.totalButton.Size = new System.Drawing.Size(192, 41);
            this.totalButton.TabIndex = 8;
            this.totalButton.Text = "Get Total";
            this.totalButton.UseVisualStyleBackColor = true;
            this.totalButton.Click += new System.EventHandler(this.totalButton_Click);
            // 
            // avgButton
            // 
            this.avgButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgButton.Location = new System.Drawing.Point(471, 100);
            this.avgButton.Name = "avgButton";
            this.avgButton.Size = new System.Drawing.Size(192, 41);
            this.avgButton.TabIndex = 9;
            this.avgButton.Text = "Get Average";
            this.avgButton.UseVisualStyleBackColor = true;
            this.avgButton.Click += new System.EventHandler(this.avgButton_Click);
            // 
            // avgLabel
            // 
            this.avgLabel.AutoSize = true;
            this.avgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgLabel.Location = new System.Drawing.Point(342, 165);
            this.avgLabel.Name = "avgLabel";
            this.avgLabel.Size = new System.Drawing.Size(123, 31);
            this.avgLabel.TabIndex = 10;
            this.avgLabel.Text = "Average:";
            // 
            // highestLabel
            // 
            this.highestLabel.AutoSize = true;
            this.highestLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highestLabel.Location = new System.Drawing.Point(740, 165);
            this.highestLabel.Name = "highestLabel";
            this.highestLabel.Size = new System.Drawing.Size(115, 31);
            this.highestLabel.TabIndex = 11;
            this.highestLabel.Text = "Highest:";
            // 
            // lowestLabel
            // 
            this.lowestLabel.AutoSize = true;
            this.lowestLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowestLabel.Location = new System.Drawing.Point(1094, 165);
            this.lowestLabel.Name = "lowestLabel";
            this.lowestLabel.Size = new System.Drawing.Size(109, 31);
            this.lowestLabel.TabIndex = 12;
            this.lowestLabel.Text = "Lowest:";
            // 
            // avgTextBox
            // 
            this.avgTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgTextBox.Location = new System.Drawing.Point(471, 162);
            this.avgTextBox.Name = "avgTextBox";
            this.avgTextBox.Size = new System.Drawing.Size(192, 38);
            this.avgTextBox.TabIndex = 13;
            // 
            // highestTextBox
            // 
            this.highestTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highestTextBox.Location = new System.Drawing.Point(861, 162);
            this.highestTextBox.Name = "highestTextBox";
            this.highestTextBox.Size = new System.Drawing.Size(192, 38);
            this.highestTextBox.TabIndex = 14;
            // 
            // lowestTextBox
            // 
            this.lowestTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowestTextBox.Location = new System.Drawing.Point(1209, 162);
            this.lowestTextBox.Name = "lowestTextBox";
            this.lowestTextBox.Size = new System.Drawing.Size(192, 38);
            this.lowestTextBox.TabIndex = 15;
            // 
            // highestButton
            // 
            this.highestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highestButton.Location = new System.Drawing.Point(861, 100);
            this.highestButton.Name = "highestButton";
            this.highestButton.Size = new System.Drawing.Size(192, 41);
            this.highestButton.TabIndex = 16;
            this.highestButton.Text = "Get Highest";
            this.highestButton.UseVisualStyleBackColor = true;
            this.highestButton.Click += new System.EventHandler(this.highestButton_Click);
            // 
            // lowestButton
            // 
            this.lowestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowestButton.Location = new System.Drawing.Point(1209, 100);
            this.lowestButton.Name = "lowestButton";
            this.lowestButton.Size = new System.Drawing.Size(192, 41);
            this.lowestButton.TabIndex = 17;
            this.lowestButton.Text = "Get Lowest";
            this.lowestButton.UseVisualStyleBackColor = true;
            this.lowestButton.Click += new System.EventHandler(this.lowestButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1437, 310);
            this.Controls.Add(this.lowestButton);
            this.Controls.Add(this.highestButton);
            this.Controls.Add(this.lowestTextBox);
            this.Controls.Add(this.highestTextBox);
            this.Controls.Add(this.avgTextBox);
            this.Controls.Add(this.lowestLabel);
            this.Controls.Add(this.highestLabel);
            this.Controls.Add(this.avgLabel);
            this.Controls.Add(this.avgButton);
            this.Controls.Add(this.totalButton);
            this.Controls.Add(this.totalTextBox);
            this.Controls.Add(this.totalLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.TextBox totalTextBox;
        private System.Windows.Forms.Button totalButton;
        private System.Windows.Forms.Button avgButton;
        private System.Windows.Forms.Label avgLabel;
        private System.Windows.Forms.Label highestLabel;
        private System.Windows.Forms.Label lowestLabel;
        private System.Windows.Forms.TextBox avgTextBox;
        private System.Windows.Forms.TextBox highestTextBox;
        private System.Windows.Forms.TextBox lowestTextBox;
        private System.Windows.Forms.Button highestButton;
        private System.Windows.Forms.Button lowestButton;
    }
}

