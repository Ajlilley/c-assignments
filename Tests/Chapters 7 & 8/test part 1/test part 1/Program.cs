﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_part_1
{
    class Program
    {
        static void Main(string[] args)
        {
            double highest, lowest, sum, average;
            double[] fillArray = new double[15];
            PromptNumbers(fillArray);
            CalculateStatistics(fillArray, out highest, out lowest, out sum, out average);
            WriteLine("The maximum value is" + highest + "\nThe lowest minimum value is" + lowest + "\nThe sum is" + sum + "\nThe average is " + average);
        }
        public static void PromptNumbers(double[] fillArray)
        {

            for (int i = 0; i < 15; i++)
            {
                // user input
                WriteLine("enter up to 15 numbers");
                int var;

                while (!(Int32.TryParse(ReadLine(), out var)) && (!((Convert.ToBoolean(ReadLine())))))

                {
                    WriteLine("invalid input! Please enter a number!");
                    WriteLine("enter up to 15 numbers");
                }
                if (ReadLine() == "z")
                {
                    fillArray[i] = var;
                    
                }
                else
                {
                    fillArray[i] = var;

                }

            }
        }
        public static void CalculateStatistics(double[] fillArray, out double highest, out double lowest, out double sum, out double average)

        {

            sum = 0;
            double high = Double.MinValue;
            double low = Double.MaxValue;
            int numsEntered = 0;
            double sumnums = 0;
            for (int i = 0; i < fillArray.Length; ++i)
            {
                if (fillArray[i] != 0)
                {
                    sumnums += fillArray[i];
                    ++numsEntered;
                    if (fillArray[i] > high)
                    {
                        high = fillArray[i];
                    }
                    if (fillArray[i] < low)
                    {
                        low = fillArray[i];
                    }
                }
            }
            highest = high;
            lowest = low;
            sum = sumnums;
            average = sumnums / numsEntered;
            ReadLine();

        }
    }
}
