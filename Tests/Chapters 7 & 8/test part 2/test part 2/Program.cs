﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_part_2
{
    class Program
    {
        static void Main()
        {
            int highest, lowest, sum, average;
            int[] listArray = new int[15];
            PromptNumbers(listArray);
            CalculateStatistics(listArray, out highest, out lowest, out sum, out average);
            WriteLine("The maximum value is " + highest + "\nThe minimum value is " + lowest + "\nThe sum is " + sum + "\nThe average is " + average);
        }
        public static void PromptNumbers(int[] listArray)
        {

            for (int i = 0; i < 15; ++i)
            {
                // user input
                WriteLine("enter up to 15 numbers");
                string input = Console.ReadLine(); //get the input
                int num = i;

                if (!int.TryParse(input, out num) & (!(Convert.ToBoolean(input == "z"))))
                {
                    WriteLine("invalid input - Please enter a number");
                    WriteLine(" enter up to 15 numbers");
                }
                if (input == "z")
                {
                    int highest, lowest, sum, average;
                    CalculateStatistics(listArray, out highest, out lowest, out sum, out average);
                    WriteLine("The maximum value is " + highest + "\nThe minimum value is " + lowest + "\nThe sum is " + sum + "\nThe average is " + average);

                }
                if (i == 14)
                {
                    int highest, lowest, sum, average;
                    CalculateStatistics(listArray, out highest, out lowest, out sum, out average);
                    WriteLine("The maximum value is " + highest + "\nThe minimum value is " + lowest + "\nThe sum is " + sum + "\nThe average is " + average);
                    return;

                }
                else
                {
                    listArray[i] = num;

                }

            }
        }
        public static void CalculateStatistics(int[] listArray, out int highest, out int lowest, out int sum, out int average)

        {

            sum = 0;
            int high = Int32.MinValue;
            int low = Int32.MaxValue;
            int numsEntered = 0;
            int sumnums = 0;
            for (int i = 0; i < listArray.Length; ++i)
            {
                if (listArray[i] != 0)
                {
                    sumnums += listArray[i];
                    ++numsEntered;
                    if (listArray[i] > high)
                    {
                        high = listArray[i];
                    }//end of if 
                    if (listArray[i] < low)
                    {
                        low = listArray[i];
                    }
                }
            }
            highest = high;
            lowest = low;
            sum = sumnums;
            average = sumnums / numsEntered;
            ReadLine();
        }
    }
}
