﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckZips
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayZip = { 63120, 63121, 63122, 63123, 63124, 63125, 63126, 63127, 63128, 63129};

            //Console.WriteLine("Enter in your area code");
            //int userInput = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter in your area code");
            int userInput = (Convert.ToInt32(Console.ReadLine()));

            if (userInput == arrayZip[0] || userInput == arrayZip[1] || userInput == arrayZip[2] || userInput == arrayZip[3] || userInput == arrayZip[4] || userInput == arrayZip[5] || userInput == arrayZip[6] || userInput == arrayZip[7] || userInput == arrayZip[8] || userInput == arrayZip[9])
            {
                Console.WriteLine("Your company is in our delivery area!");
                Console.ReadLine();
            }

            else
            {
                Console.WriteLine("Sorry but, your company is not in our delivery area!");
                Console.ReadLine();
            }
        }
    }
}
