﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureList
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare variables
            int[] temp = new int[7];
            int userInput = 0;

            //Get user input

            Console.WriteLine("Enter in the highest temperature for 7 consecutive days.");

            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = Convert.ToInt32(Console.ReadLine());
                userInput = userInput + 1;

                int average = (temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5] + temp[6]) / 7;

                if (userInput == 7)
                {
                    foreach (int day in temp)
                        Console.WriteLine(day.ToString() + " is " + (Math.Abs(average - day)) +  " degrees from the average temperature.");
                        //Console.WriteLine("The average temperature is " + average + " degrees.");
                        //Console.WriteLine(temp[i]);
                        Console.ReadLine();
                }
            }
        }
    }
}
