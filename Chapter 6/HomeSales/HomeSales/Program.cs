﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSales
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] employeeArray = { "Danielle", "Edward", "Francis" };
            string[] initialsArray = { "D", "E", "F" };
            double[] totalSalesArray = { 0.0, 0.0, 0.0 };
            string userInput = "";

            while (userInput != "z")
            {
                Console.Write("Enter in sales person D, E, or F: ");
                userInput = (Console.ReadLine());

                if (userInput.ToLower() != "z")
                {
                    switch (userInput.ToLower())
                    {
                        case "d":
                            Console.Write("enter a sales amount for Danielle: ");
                            totalSalesArray[0] += Convert.ToDouble(Console.ReadLine());
                            break;

                        case "e":
                            Console.Write("enter a sales amount for Edward: ");
                            totalSalesArray[1] += Convert.ToDouble(Console.ReadLine());
                            break;

                        case "f":
                            Console.Write("enter a sales amount for Francis: ");
                            totalSalesArray[2] += Convert.ToDouble(Console.ReadLine());
                            break;
                        default:
                            Console.Write("Invaild value entered. Please try D, E, or F.");
                            break;
                    }
                }
            }

            Console.WriteLine("Danielle's sales total is " + totalSalesArray[0].ToString("C"));
            Console.WriteLine("Edward's sales total is " + totalSalesArray[1].ToString("C"));
            Console.WriteLine("Francis' sales total is " + totalSalesArray[2].ToString("C"));

            if (totalSalesArray[0] > totalSalesArray[1] && totalSalesArray[0] > totalSalesArray[2])
            {
                Console.Write("Danielle has the highest sales total at: " + totalSalesArray[0].ToString("C"));
            }
            else if (totalSalesArray[2] > totalSalesArray[0] && totalSalesArray[2] > totalSalesArray[1])
            {
                Console.Write("Francis has the highest sales total at: " + totalSalesArray[2].ToString("C"));
            }
            else if (totalSalesArray[1] > totalSalesArray[0] && totalSalesArray[1] > totalSalesArray[2])
            {
                Console.Write("Edward has the highest sales total at: " + totalSalesArray[1].ToString("C"));
            }
            Console.ReadLine();
        }
    }
}
