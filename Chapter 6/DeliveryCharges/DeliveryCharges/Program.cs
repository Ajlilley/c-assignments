﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryCharges
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] arrayZip = { { 63120, 6.00 }, { 63121, 2.35 }, { 63122, 8.53 }, { 63123, 3.82 }, { 63124, 1.50 }, { 63125, 7.33 }, { 63126, 5.25 }, { 63127, 1.23 }, { 63128, 5.12 }, { 63129, 3.32 } };
            double userInput;

            //Ask user for zip code
            Console.WriteLine("Please enter a ZIP code to view the delivery charge for that area.");
            userInput = Convert.ToDouble(Console.ReadLine());

            int zip;

            for (zip = 0; zip < arrayZip.Length / 2; zip++)
            {
                if (arrayZip[zip, 0] == userInput)
                {
                    break;
                }
            }



            if (zip == arrayZip.Length / 2)
            {
                Console.WriteLine("ZIP Code {0} is not in our delivery area.", userInput);
            }
            else
            {
                Console.WriteLine("The price of delivery to ZIP code {0} is ${1}", userInput, arrayZip[zip, 1]);
            }
            Console.ReadLine();
        }
    }
}
       