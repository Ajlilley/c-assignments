﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] scores = new int[4];
            int userInput = 0;

            Console.WriteLine("Enter in 4 integer quiz scores in consecutive order.");

            for (int i = 0; i < scores.Length; i++)
            {
                scores[i] = Convert.ToInt32(Console.ReadLine());
                userInput = userInput + 1;

                while (userInput == 4)
                {

                    if (scores[0] < scores[1] && scores[0] < scores[2] && scores[0] < scores[3] && scores[1] < scores[2] && scores[1] < scores[3] && scores[2] < scores[3])
                    {
                        Console.WriteLine();
                        Console.WriteLine(scores[0] + "%, " + scores[1] + "%, " + scores[2] + "%, " + scores[3] + "%");
                        Console.WriteLine("Congratulations on making improvments on your quiz scores!");
                        Console.ReadLine();
                        return;
                    }

                    else if (scores[3] < scores[2] && scores[2] < scores[1] && scores[1] < scores[0])
                    {
                        Console.WriteLine();
                        Console.WriteLine(scores[0] + "%, " + scores[1] + "%, " + scores[2] + "%, " + scores[3] + "%");
                        Console.WriteLine("You're quiz scores are slipping, you need to study more!");
                        Console.WriteLine("A more desirable order for your scores would be " + scores[3] + "%, " + scores[2] + "%, " + scores[1] + "%, " + scores[0] + "%");
                        Console.ReadLine();
                        return;
                    }

                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine(scores[0] + "%, " + scores[1] + "%, " + scores[2] + "%, " + scores[3] + "%");
                        Console.WriteLine("You're quiz scores are acceptable but, perhaps consider studying just a bit more.");
                        Console.ReadLine();
                        return;
                    }
                }
            }
        }
    }
}