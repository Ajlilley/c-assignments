﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare variables
            double[] arrayVal = { 100, 200, 300, 400, 500, 600, 700, 800 };
            int SentVal = 1;

            //Get user input
            while (SentVal != -99)
            {
                Console.WriteLine("Enter 1 to view the list in numeric order. Enter 2 to view the list in reverse numeric order. Enter 3 to choose a specific position to view. Enter -99 to end the program.");
                SentVal = (Convert.ToInt32(Console.ReadLine()));

                if (SentVal == 1)
                {
                    foreach (double value in arrayVal)
                        Console.WriteLine(value);
                        Console.WriteLine();
                }

                else if (SentVal == 2)
                {
                    Array.Reverse(arrayVal);
                    foreach (double dblInt in arrayVal)
                        Console.WriteLine(dblInt);
                        Console.WriteLine();
                    Array.Reverse(arrayVal);
                }

                else if (SentVal == 3)
                {
                    int index = int.Parse(Console.ReadLine());
                    Console.WriteLine("Number {0} in the list is {1}", index, arrayVal[index - 1]);
                    Console.WriteLine();
                }
            }
        }
    }
}
