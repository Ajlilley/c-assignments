﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            string zipCodeInput;
            string minutesInput;
            int zipCodeEntry;
            int minutesEntry;
            double price = 0;
            double total = 0;
            bool validZip = false;
            int[] zipcodes = { 63120, 63121, 63122, 63123, 63124, 63125 };
            double[] rates = { 0.05, 0.14, 0.07, 0.21, 0.13, 0.18 };

            Write("Enter a zipcode >> ");
            zipCodeInput = ReadLine();
            zipCodeEntry = Convert.ToInt32(zipCodeInput);
            Write("Enter length of call in minutes >> ");
            minutesInput = ReadLine();
            minutesEntry = Convert.ToInt32(minutesInput);

            x = 0;
            while (x < zipcodes.Length && zipCodeEntry != zipcodes[x])
                ++x;

            if (x != zipcodes.Length)
            {
                validZip = true;
                price = rates[x];
                total = rates[x] * minutesEntry;
            }
            if (validZip)
                WriteLine("You spoke on zipcode {0} for {1} minutes. Your total is {2}", zipCodeEntry, minutesEntry, total.ToString("C2"));
            else
                WriteLine("Zipcode {0} is not within the delivery area!! ", zipCodeEntry);



            ReadLine();

        }
    }
}
