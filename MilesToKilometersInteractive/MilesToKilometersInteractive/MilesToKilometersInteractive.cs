﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilesToKilometersInteractive
{
    class MilesToKilometersInteractive
    {
        static void Main(string[] args)
        {
            float kilometers = 1.6f;
            int num;
            Console.WriteLine("Enter the number of miles: ");
            num = Convert.ToInt32(Console.ReadLine());
            double MilesToKilo = num * kilometers;
            Console.WriteLine("{0}" + " mile(s) is " + "{1}" + " kilometer(s)", num, MilesToKilo);
            Console.ReadLine();
        }
    }
}
