﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numLabel1 = new System.Windows.Forms.Label();
            this.numLabel2 = new System.Windows.Forms.Label();
            this.addSumButton1 = new System.Windows.Forms.Button();
            this.numTextBox1 = new System.Windows.Forms.TextBox();
            this.numTextBox2 = new System.Windows.Forms.TextBox();
            this.sumLabel1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // numLabel1
            // 
            this.numLabel1.AutoSize = true;
            this.numLabel1.Location = new System.Drawing.Point(66, 60);
            this.numLabel1.Name = "numLabel1";
            this.numLabel1.Size = new System.Drawing.Size(79, 13);
            this.numLabel1.TabIndex = 0;
            this.numLabel1.Text = "Enter a number";
            // 
            // numLabel2
            // 
            this.numLabel2.AutoSize = true;
            this.numLabel2.Location = new System.Drawing.Point(66, 118);
            this.numLabel2.Name = "numLabel2";
            this.numLabel2.Size = new System.Drawing.Size(109, 13);
            this.numLabel2.TabIndex = 1;
            this.numLabel2.Text = "Enter another number";
            // 
            // addSumButton1
            // 
            this.addSumButton1.Location = new System.Drawing.Point(69, 181);
            this.addSumButton1.Name = "addSumButton1";
            this.addSumButton1.Size = new System.Drawing.Size(75, 23);
            this.addSumButton1.TabIndex = 2;
            this.addSumButton1.Text = "Click to add";
            this.addSumButton1.UseVisualStyleBackColor = true;
            this.addSumButton1.Click += new System.EventHandler(this.addSumButton1_Click);
            // 
            // numTextBox1
            // 
            this.numTextBox1.Location = new System.Drawing.Point(234, 60);
            this.numTextBox1.Name = "numTextBox1";
            this.numTextBox1.Size = new System.Drawing.Size(100, 20);
            this.numTextBox1.TabIndex = 3;
            // 
            // numTextBox2
            // 
            this.numTextBox2.Location = new System.Drawing.Point(234, 118);
            this.numTextBox2.Name = "numTextBox2";
            this.numTextBox2.Size = new System.Drawing.Size(100, 20);
            this.numTextBox2.TabIndex = 4;
            // 
            // sumLabel1
            // 
            this.sumLabel1.AutoSize = true;
            this.sumLabel1.Location = new System.Drawing.Point(231, 181);
            this.sumLabel1.Name = "sumLabel1";
            this.sumLabel1.Size = new System.Drawing.Size(0, 13);
            this.sumLabel1.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 333);
            this.Controls.Add(this.sumLabel1);
            this.Controls.Add(this.numTextBox2);
            this.Controls.Add(this.numTextBox1);
            this.Controls.Add(this.addSumButton1);
            this.Controls.Add(this.numLabel2);
            this.Controls.Add(this.numLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label numLabel1;
        private System.Windows.Forms.Label numLabel2;
        private System.Windows.Forms.Button addSumButton1;
        private System.Windows.Forms.TextBox numTextBox1;
        private System.Windows.Forms.TextBox numTextBox2;
        private System.Windows.Forms.Label sumLabel1;
    }
}

