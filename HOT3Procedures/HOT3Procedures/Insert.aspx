﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Insert.aspx.cs" Inherits="HOT3Procedures.Insert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>HOT3 Test</h1>
            <h2>CRUD Operations</h2>
            <h3>Insert Data Into a Table</h3>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HOT3.mdfConnectionString %>" InsertCommand="spInsertCustomer" InsertCommandType="StoredProcedure" SelectCommand="spGetCustomerData" SelectCommandType="StoredProcedure">
                <InsertParameters>
                    <asp:Parameter Name="CustomerFirstName" Type="String" />
                    <asp:Parameter Name="CustomerMiddleName" Type="String" />
                    <asp:Parameter Name="CustomerLastName" Type="String" />
                    <asp:Parameter Name="CustomerEmail" Type="String" />
                    <asp:Parameter Name="CustomerDOB" Type="String" />
                    <asp:Parameter Name="CustomerSSN" Type="String" />
                </InsertParameters>
            </asp:SqlDataSource>
            <span>First Name:</span><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            <br />
            <span>Last Name:</span><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="butnSubmit" runat="server" Text="Create New Customer" OnClick="butnSubmit_Click" />
            <br />
            <asp:Label ID="lblResult" runat="server" ForeColor="Red"></asp:Label><br />
            <a href="Read.aspx">Read</a> | <a href="Insert.aspx">Insert</a> | <a href="Update.aspx">Update</a> | <a href="Delete.aspx">Delete</a></div>
        </div>
    </form>
</body>
</html>
