﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HOT3Procedures
{
    public partial class Insert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void butnSubmit_Click(object sender, EventArgs e)
        {
            SqlDataSource1.InsertParameters["CustomerFirstName"].DefaultValue = txtFirstName.Text;
            SqlDataSource1.InsertParameters["CustomerLastName"].DefaultValue = txtLastName.Text;

            try
            {
                SqlDataSource1.Insert();
                lblResult.Text = "Customer Created!";
            }
            catch(Exception ex)
            {
                lblResult.Text = "Please enter a first name AND last name to create a customer!";
            }
        }
    }
}