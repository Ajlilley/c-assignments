﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentGPA
{
    static class DemoStudentEvent
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Student oneStu = new Student();
            EventListener listener = new EventListener(oneStu);
            oneStu.IdNum = 2345;
            oneStu.IdNum = 4567;
            oneStu.Gpa = 3.2;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
