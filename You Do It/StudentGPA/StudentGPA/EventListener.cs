﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace StudentGPA
{
    class EventListener
    {
        private Student stu;
        public EventListener(Student student)
        {
            stu = student;stu.Changed += new ChangedEventHandler(StudentChanged);
        }
        private void StudentChanged(object sender, EventArgs e)
        {
            WriteLine("The student has changed.");
            WriteLine("ID# {0} GPA {1}", stu.IdNum, stu.Gpa);
            ReadLine();
        }
    }
}
