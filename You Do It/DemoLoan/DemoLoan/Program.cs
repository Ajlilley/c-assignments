﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLoan
{
    class DemoCarLoan
    {
        static void Main(string[] args)
        {
            Loan aLoan = new Loan();
            aLoan.LoanNumber = 2239;
            //Console.WriteLine("{0}", aLoan.LoanNumber);
            //Console.ReadLine();
            aLoan.LastName = "Mitchelle";
            aLoan.LoanAmount = 1000.00;
            Console.WriteLine("Loan #{0} for {1:C} is for {2}", aLoan.LoanNumber, aLoan.LoanAmount, aLoan.LastName);
            Console.ReadLine();
        }
    }
}
