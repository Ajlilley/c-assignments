﻿namespace TestPractice
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.subscriptionPlanLabel = new System.Windows.Forms.Label();
            this.commercialFreeRadioButton = new System.Windows.Forms.RadioButton();
            this.limitedCommercialsRadioButton = new System.Windows.Forms.RadioButton();
            this.withCommercialsRadioButton = new System.Windows.Forms.RadioButton();
            this.classicCheckBox = new System.Windows.Forms.CheckBox();
            this.countryCheckBox = new System.Windows.Forms.CheckBox();
            this.jazzCheckBox = new System.Windows.Forms.CheckBox();
            this.rockCheckBox = new System.Windows.Forms.CheckBox();
            this.genreLabel = new System.Windows.Forms.Label();
            this.totalLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(178, 25);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(488, 55);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Internet Radio Station";
            // 
            // subscriptionPlanLabel
            // 
            this.subscriptionPlanLabel.AutoSize = true;
            this.subscriptionPlanLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subscriptionPlanLabel.Location = new System.Drawing.Point(107, 107);
            this.subscriptionPlanLabel.Name = "subscriptionPlanLabel";
            this.subscriptionPlanLabel.Size = new System.Drawing.Size(174, 32);
            this.subscriptionPlanLabel.TabIndex = 1;
            this.subscriptionPlanLabel.Text = "Select the subscription plan \r\nyou prefer";
            // 
            // commercialFreeRadioButton
            // 
            this.commercialFreeRadioButton.AutoSize = true;
            this.commercialFreeRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commercialFreeRadioButton.Location = new System.Drawing.Point(110, 188);
            this.commercialFreeRadioButton.Name = "commercialFreeRadioButton";
            this.commercialFreeRadioButton.Size = new System.Drawing.Size(123, 20);
            this.commercialFreeRadioButton.TabIndex = 2;
            this.commercialFreeRadioButton.TabStop = true;
            this.commercialFreeRadioButton.Text = "commercial-free";
            this.commercialFreeRadioButton.UseVisualStyleBackColor = true;
            // 
            // limitedCommercialsRadioButton
            // 
            this.limitedCommercialsRadioButton.AutoSize = true;
            this.limitedCommercialsRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.limitedCommercialsRadioButton.Location = new System.Drawing.Point(110, 237);
            this.limitedCommercialsRadioButton.Name = "limitedCommercialsRadioButton";
            this.limitedCommercialsRadioButton.Size = new System.Drawing.Size(145, 20);
            this.limitedCommercialsRadioButton.TabIndex = 3;
            this.limitedCommercialsRadioButton.TabStop = true;
            this.limitedCommercialsRadioButton.Text = "limited commercials";
            this.limitedCommercialsRadioButton.UseVisualStyleBackColor = true;
            // 
            // withCommercialsRadioButton
            // 
            this.withCommercialsRadioButton.AutoSize = true;
            this.withCommercialsRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.withCommercialsRadioButton.Location = new System.Drawing.Point(110, 286);
            this.withCommercialsRadioButton.Name = "withCommercialsRadioButton";
            this.withCommercialsRadioButton.Size = new System.Drawing.Size(128, 20);
            this.withCommercialsRadioButton.TabIndex = 4;
            this.withCommercialsRadioButton.TabStop = true;
            this.withCommercialsRadioButton.Text = "with commercials";
            this.withCommercialsRadioButton.UseVisualStyleBackColor = true;
            // 
            // classicCheckBox
            // 
            this.classicCheckBox.AutoSize = true;
            this.classicCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classicCheckBox.Location = new System.Drawing.Point(572, 191);
            this.classicCheckBox.Name = "classicCheckBox";
            this.classicCheckBox.Size = new System.Drawing.Size(69, 20);
            this.classicCheckBox.TabIndex = 5;
            this.classicCheckBox.Text = "classic";
            this.classicCheckBox.UseVisualStyleBackColor = true;
            // 
            // countryCheckBox
            // 
            this.countryCheckBox.AutoSize = true;
            this.countryCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countryCheckBox.Location = new System.Drawing.Point(572, 240);
            this.countryCheckBox.Name = "countryCheckBox";
            this.countryCheckBox.Size = new System.Drawing.Size(70, 20);
            this.countryCheckBox.TabIndex = 6;
            this.countryCheckBox.Text = "country";
            this.countryCheckBox.UseVisualStyleBackColor = true;
            // 
            // jazzCheckBox
            // 
            this.jazzCheckBox.AutoSize = true;
            this.jazzCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jazzCheckBox.Location = new System.Drawing.Point(572, 289);
            this.jazzCheckBox.Name = "jazzCheckBox";
            this.jazzCheckBox.Size = new System.Drawing.Size(54, 20);
            this.jazzCheckBox.TabIndex = 7;
            this.jazzCheckBox.Text = "Jazz";
            this.jazzCheckBox.UseVisualStyleBackColor = true;
            // 
            // rockCheckBox
            // 
            this.rockCheckBox.AutoSize = true;
            this.rockCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rockCheckBox.Location = new System.Drawing.Point(572, 340);
            this.rockCheckBox.Name = "rockCheckBox";
            this.rockCheckBox.Size = new System.Drawing.Size(59, 20);
            this.rockCheckBox.TabIndex = 8;
            this.rockCheckBox.Text = "Rock";
            this.rockCheckBox.UseVisualStyleBackColor = true;
            this.rockCheckBox.CheckedChanged += new System.EventHandler(this.rockCheckBox_CheckedChanged);
            // 
            // genreLabel
            // 
            this.genreLabel.AutoSize = true;
            this.genreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genreLabel.Location = new System.Drawing.Point(569, 107);
            this.genreLabel.Name = "genreLabel";
            this.genreLabel.Size = new System.Drawing.Size(175, 48);
            this.genreLabel.TabIndex = 9;
            this.genreLabel.Text = "Select the music genres you\r\nwant for your personalized\r\nmusic station";
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.Location = new System.Drawing.Point(283, 422);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(116, 37);
            this.totalLabel.TabIndex = 10;
            this.totalLabel.Text = "Total $";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 521);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.genreLabel);
            this.Controls.Add(this.rockCheckBox);
            this.Controls.Add(this.jazzCheckBox);
            this.Controls.Add(this.countryCheckBox);
            this.Controls.Add(this.classicCheckBox);
            this.Controls.Add(this.withCommercialsRadioButton);
            this.Controls.Add(this.limitedCommercialsRadioButton);
            this.Controls.Add(this.commercialFreeRadioButton);
            this.Controls.Add(this.subscriptionPlanLabel);
            this.Controls.Add(this.titleLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label subscriptionPlanLabel;
        private System.Windows.Forms.RadioButton commercialFreeRadioButton;
        private System.Windows.Forms.RadioButton limitedCommercialsRadioButton;
        private System.Windows.Forms.RadioButton withCommercialsRadioButton;
        private System.Windows.Forms.CheckBox classicCheckBox;
        private System.Windows.Forms.CheckBox countryCheckBox;
        private System.Windows.Forms.CheckBox jazzCheckBox;
        private System.Windows.Forms.CheckBox rockCheckBox;
        private System.Windows.Forms.Label genreLabel;
        private System.Windows.Forms.Label totalLabel;
    }
}

