﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Greeting
{
    delegate void GreetingDelegate(string s);

    class Program
    {
        public static void Hello(string s)
        {
            WriteLine("Hello, {0}", s);
        }

        public static void Goodbye(string s)
        {
            WriteLine("Goodbye, {0}", s);
        }

        static void Main()
        {
            GreetingDelegate firstDel, secondDel;
            firstDel = new GreetingDelegate(Hello);
            secondDel = new GreetingDelegate(Goodbye);
            firstDel += secondDel;
            GreetMethod(firstDel, "Cathy");
            GreetMethod(secondDel, "Bob");
        }
        public static void GreetMethod(GreetingDelegate gd, string name)
        {
            WriteLine("The greeting is:");
            gd(name);
            ReadLine();
        }
    }
}
