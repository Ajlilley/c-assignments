﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ExceptionsOnPurpose
{
    class Program
    {
        static void Main(string[] args)
        {
            int answer= 0;
            int result;
            int zero = 0;
            try
            {
                Write("Enter an integer >> ");
                answer = Convert.ToInt32(ReadLine());
                result = answer / zero;
            }

            catch(Exception e)
            {
                WriteLine(e.Message);
            }
            WriteLine("The answer is " + answer);
            Console.ReadLine();
        }
    }
}
