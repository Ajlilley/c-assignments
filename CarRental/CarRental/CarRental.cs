﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    class CarRental
    {
        static void Main(string[] args)
        {
            int daysrented;
            double miles;
            Console.WriteLine("Enter the number of days you rented the car here:");
            daysrented = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the number of miles you drove the car here:");
            miles = Convert.ToDouble(Console.ReadLine());
            double TotalCharge = daysrented * 20 + miles * .25;
            Console.WriteLine("Your total for the car rental comes out to be " + "{0:C2}", TotalCharge);
            Console.ReadLine();
        }
    }
}
