﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoubleDecimalTest
{
    class DoubleDecimalTest
    {
        static void Main(string[] args)
        {
            double w = 13.1234567891234567;
            decimal q = 13.123456789123456789M;
            Console.WriteLine(w);
            Console.WriteLine(q);
            Console.ReadLine();
        }
    }
}
