﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectDataSourceTest.App_Code
{
    public class PhoneNumber
    {
        public string PhoneNumberID { get; set; }
        public string PhoneNumberTypeID { get; set; }
        public string AreaCode { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string Extention { get; set; }
    }
}