﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Collections;

namespace ObjectDataSourceTest.App_Code
{
    [DataObject(true)]
    public class PhoneNumberDB
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static List<PhoneNumber> GetCategories()
        {
            List<PhoneNumber> categoryList = new List<PhoneNumber>();
            string sql = "SELECT PhoneNumberID, PhoneNumberTypeID, AreaCode, Prefix, Suffix, Extention "
                + "FROM PhoneNumber ORDER BY PhoneNumberID";
            using (SqlConnection con = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    PhoneNumber phonenumber;
                    while (dr.Read())
                    {
                        phonenumber = new PhoneNumber();
                        phonenumber.PhoneNumberID = dr["PhoneNumberID"].ToString();
                        phonenumber.PhoneNumberTypeID = dr["PhoneNumberTypeID"].ToString();
                        phonenumber.AreaCode = dr["AreaCode"].ToString();
                        phonenumber.Prefix = dr["Prefix"].ToString();
                        phonenumber.Suffix = dr["Suffix"].ToString();
                        phonenumber.Extention = dr["Extention"].ToString();

                        categoryList.Add(phonenumber);
                    }
                    dr.Close();
                }
            }
            return categoryList;
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public static void InsertPhoneNumber(PhoneNumber phonenumber)
        {
            string sql = "INSERT INTO PhoneNumber "
                + "(PhoneNumberID, PhoneNumberTypeID, AreaCode, Prefix, Suffix, Extention) "
                + "VALUES (@PhoneNumberID, @PhoneNumberTypeID, @AreaCode, @Prefix, @Suffix, @Extention)";
            using (SqlConnection con = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("PhoneNumberID", phonenumber.PhoneNumberID);
                    cmd.Parameters.AddWithValue("PhoneNumberTypeID", phonenumber.PhoneNumberTypeID);
                    cmd.Parameters.AddWithValue("AreaCode", phonenumber.AreaCode);
                    cmd.Parameters.AddWithValue("Prefix", phonenumber.Prefix);
                    cmd.Parameters.AddWithValue("Suffix", phonenumber.Suffix);
                    cmd.Parameters.AddWithValue("Extention", phonenumber.Extention);
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        [DataObjectMethod(DataObjectMethodType.Delete)]
        public static int DeletePhoneNumber(PhoneNumber phonenumber)
        {
            int deleteCount = 0;
            string sql = "DELETE FROM PhoneNumber "
                + "WHERE PhoneNumberID = @PhoneNumberID "
                + "AND PhoneNumberTypeID = @PhoneNumberTypeID "
                + "AND AreaCode = @AreaCode "
                + "AND Prefix = @Prefix "
                + "AND Suffix = @Suffix "
                + "AND Extention = @Extention";
            using (SqlConnection con = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("PhoneNumberID", phonenumber.PhoneNumberID);
                    cmd.Parameters.AddWithValue("PhoneNumberTypeID", phonenumber.PhoneNumberTypeID);
                    cmd.Parameters.AddWithValue("AreaCode", phonenumber.AreaCode);
                    cmd.Parameters.AddWithValue("Prefix", phonenumber.Prefix);
                    cmd.Parameters.AddWithValue("Suffix", phonenumber.Suffix);
                    cmd.Parameters.AddWithValue("Extention", phonenumber.Extention);
                    con.Open();
                    deleteCount = cmd.ExecuteNonQuery();
                }
            }
            return deleteCount;
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public static int UpdatePhoneNumber(PhoneNumber original_PhoneNumber,
            PhoneNumber phonenumber)
        {
            int updateCount = 0;
            string sql = "UPDATE PhoneNumber "
                + "SET PhoneNumberTypeID = @PhoneNumberTypeID, "
                + "AreaCode = @AreaCode "
                + "Prefix = @Prefix "
                + "Suffix = @Suffix "
                + "Extention = @Extention "
                + "WHERE PhoneNumberID = @original_PhoneNumberID "
                + "AND PhoneNumberTypeID = @original_PhoneNumberTypeID "
                + "AND AreaCode = @original_AreaCode "
                + "AND Prefix = @original_Prefix "
                + "AND Suffix = @original_Suffix "
                + "AND Extention = @original_Extention";
            using (SqlConnection con = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("PhoneNumberTypeID", phonenumber.PhoneNumberTypeID);
                    cmd.Parameters.AddWithValue("AreaCode", phonenumber.AreaCode);
                    cmd.Parameters.AddWithValue("Prefix", phonenumber.Prefix);
                    cmd.Parameters.AddWithValue("Suffix", phonenumber.Suffix);
                    cmd.Parameters.AddWithValue("Extention", phonenumber.Extention);
                    cmd.Parameters.AddWithValue("original_PhoneNumberID",
                        original_PhoneNumber.PhoneNumberID);
                    cmd.Parameters.AddWithValue("original_PhoneNumberTypeID",
                        original_PhoneNumber.PhoneNumberTypeID);
                    cmd.Parameters.AddWithValue("original_AreaCode",
                        original_PhoneNumber.AreaCode);
                    cmd.Parameters.AddWithValue("original_Prefix",
                        original_PhoneNumber.Prefix);
                    cmd.Parameters.AddWithValue("original_Suffix",
                        original_PhoneNumber.Suffix);
                    cmd.Parameters.AddWithValue("original_Extention",
                        original_PhoneNumber.Extention);
                    con.Open();
                    updateCount = cmd.ExecuteNonQuery();
                }
            }
            return updateCount;
        }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings
                ["ODS.mdfConnectionString"].ConnectionString;
        }
    }
}