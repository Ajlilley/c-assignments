﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhoneNumberWebForm.aspx.cs" Inherits="ObjectDataSourceTest.PhoneNumberWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1" AutoGenerateColumns="False">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="PhoneNumberID" HeaderText="PhoneNumberID" SortExpression="PhoneNumberID" />
                    <asp:BoundField DataField="PhoneNumberTypeID" HeaderText="PhoneNumberTypeID" SortExpression="PhoneNumberTypeID" />
                    <asp:BoundField DataField="AreaCode" HeaderText="AreaCode" SortExpression="AreaCode" />
                    <asp:BoundField DataField="Prefix" HeaderText="Prefix" SortExpression="Prefix" />
                    <asp:BoundField DataField="Suffix" HeaderText="Suffix" SortExpression="Suffix" />
                    <asp:BoundField DataField="Extention" HeaderText="Extention" SortExpression="Extention" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="ObjectDataSourceTest.App_Code.PhoneNumber" DeleteMethod="DeletePhoneNumber" InsertMethod="InsertPhoneNumber" OldValuesParameterFormatString="original_{0}" SelectMethod="GetCategories" TypeName="ObjectDataSourceTest.App_Code.PhoneNumberDB" UpdateMethod="UpdatePhoneNumber">
                <UpdateParameters>
                    <asp:Parameter Name="original_PhoneNumber" Type="Object" />
                    <asp:Parameter Name="phonenumber" Type="Object" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <asp:Label ID="lblError" runat="server" CssClass="text-danger" EnableViewState="False" ForeColor="Red"></asp:Label>
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="ObjectDataSource1" DefaultMode="Insert" OnItemInserted="DetailsView1_ItemInserted" CssClass="table table-bordered table-condensed">
                <Fields>
                    <asp:BoundField DataField="PhoneNumberID" HeaderText="PhoneNumberID" SortExpression="PhoneNumberID" />
                    <asp:BoundField DataField="PhoneNumberTypeID" HeaderText="PhoneNumberTypeID" SortExpression="PhoneNumberTypeID" />
                    <asp:BoundField DataField="AreaCode" HeaderText="AreaCode" SortExpression="AreaCode" />
                    <asp:BoundField DataField="Prefix" HeaderText="Prefix" SortExpression="Prefix" />
                    <asp:BoundField DataField="Suffix" HeaderText="Suffix" SortExpression="Suffix" />
                    <asp:BoundField DataField="Extention" HeaderText="Extention" SortExpression="Extention" />
                    <asp:CommandField ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
        </div>
    </form>
</body>
</html>
