﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StoredProceduresLab
{
    public partial class InsertEmployee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SqlDataSource1.InsertParameters["FirstName"].DefaultValue = txtFirstName.Text;
            SqlDataSource1.InsertParameters["LastName"].DefaultValue = txtLastName.Text;
            SqlDataSource1.InsertParameters["DeptId"].DefaultValue = DropDownList1.SelectedValue;

            try
            {
                SqlDataSource1.Insert();
                lblResult.Text = "Employee has been created!";
            }
            catch(Exception ex)
            {
                lblResult.Text = ex.Message;
            }
        }
    }
}