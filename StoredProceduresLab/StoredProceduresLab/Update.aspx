﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Update.aspx.cs" Inherits="StoredProceduresLab.Update" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Stored Procedures and SQL Data Source</h1>
            <h2>CRUD Operations</h2>
            <h3>Update Data in a Row</h3>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="EmployeeId" DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="EmployeeId" HeaderText="EmployeeId" InsertVisible="False" ReadOnly="True" SortExpression="EmployeeId" />
                    <asp:BoundField DataField="EmployeeFirstName" HeaderText="EmployeeFirstName" SortExpression="EmployeeFirstName" />
                    <asp:BoundField DataField="EmployeeLastName" HeaderText="EmployeeLastName" SortExpression="EmployeeLastName" />
                    <asp:BoundField DataField="DepartmentId" HeaderText="DepartmentId" SortExpression="DepartmentId" />
                </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RankenConnectionString %>" SelectCommand="select EmployeeId, EmployeeFirstName, EmployeeLastName, DepartmentId
from tblEmployee" UpdateCommand="spUpdateEmployee" UpdateCommandType="StoredProcedure">
                    <UpdateParameters>
                        <asp:Parameter Name="EmployeeFirstName" Type="String" />
                        <asp:Parameter Name="EmployeeLastName" Type="String" />
                        <asp:Parameter Name="DepartmentId" Type="Int32" />
                        <asp:Parameter Name="EmployeeId" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <a href="Select.aspx">Read</a> | <a href="InsertEmployee.aspx">Insert</a> | <a href="Update.aspx">Update</a> | <a href="Delete.aspx">Delete</a>

        </div>
    </form>
</body>
</html>
