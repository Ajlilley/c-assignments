namespace MVCStudent.Migrations
{
    using MVCStudent.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVCStudent.Models.StudentDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MVCStudent.Models.StudentDBContext context)
        {
            context.Students.AddOrUpdate(i => i.FirstName);
            new Student
            {
                FirstName = "Aaron",
                MiddleInitial = "J",
                LastName = "Lilley",
                StudentNumber = 13,
                Track = "IWT",
                WEG1 = "Metets Expectations",
                WEG2 = "Metets Expectations",
                WEG3 = "Metets Expectations",
                WEG4 = "Metets Expectations",
            };
        }
    }
}
