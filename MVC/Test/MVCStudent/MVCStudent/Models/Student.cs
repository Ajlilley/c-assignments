﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MVCStudent.Models
{
    public class Student
    {
        public int ID { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Required]
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }

        [StringLength(60, MinimumLength = 2)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Required]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please Enter your Badge ID")]
        [Range (100000, 99999999)]
        public int StudentNumber { get; set; }
        public string Track { get; set; }
        public string WEG1 { get; set; }
        public string WEG2 { get; set; }
        public string WEG3 { get; set; }
        public string WEG4 { get; set; }
    }

    public class StudentDBContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
    }
}