﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse3
{
    class Program
    {
        static void Main()
        {
            int firstInt = 1;
            int middleInt = 2;
            int lastInt = 3;

            WriteLine("firstInt is >> {0}", firstInt);
            WriteLine("middleInt is >> {0}", middleInt);
            WriteLine("lastInt is >> {0}", lastInt);
            WriteLine("-----------------------------------------");
            DisplayReverseReference(ref firstInt, ref middleInt, ref lastInt);
            WriteLine("firstInt is >> {0}", firstInt);
            WriteLine("middleInt is >> {0}", middleInt);
            WriteLine("lastInt is >> {0}", lastInt);
            ReadLine();
        }
        private static void DisplayReverseReference(ref int first, ref int middle, ref int last)
        {
            first = 3;
            middle = 2;
            last = 1;
        }
    }
}
