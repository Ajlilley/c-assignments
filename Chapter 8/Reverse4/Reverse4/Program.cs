﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse4
{
    class Program
    {
        static void Main()
        {
            string firstString = ("a");
            string secondString = ("b");
            string thirdString = ("c");
            string fourthString = ("d");

            WriteLine("firstString is >> {0}", firstString);
            WriteLine("secondString is >> {0}", secondString);
            WriteLine("thirdString is >> {0}", thirdString);
            WriteLine("fourthString is >> {0}", fourthString);
            WriteLine("-----------------------------------------");
            DisplayReverseReference(ref firstString, ref secondString, ref thirdString, ref fourthString);
            WriteLine("firstString is >> {0}", firstString);
            WriteLine("secondString is >> {0}", secondString);
            WriteLine("thirdString is >> {0}", thirdString);
            WriteLine("fourthString is >> {0}", fourthString);
            ReadLine();
        }
        private static void DisplayReverseReference(ref string one, ref string two, ref string three, ref string four)
        {
            one = ("d");
            two = ("c");
            three = ("b");
            four = ("a");
        }
    }
}
