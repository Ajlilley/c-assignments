﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optional_Parameters
{
    class Program
    {
        static void Main(string[] args)
        {
            double userInput1 = promptItems();
            int userInput2 = 0;
            Console.Write("If you're shopping on Monday enter (1)\n" +
                          "If you're shopping on Tuesday enter (2)\n" +
                          "If you're shopping on Wednesday enter (3)\n");
            userInput2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            switch (userInput2)
            {
                case 1:
                    Console.WriteLine("Monday's are normal price; your total comes to: " + CalculateSum(userInput1).ToString("C"));
                    break;
                case 2:
                    Console.WriteLine("Tuesday's are 50% off item cost; Your total comes to: " + CalculateSum(userInput1, cost: 10).ToString("C"));
                    break;
                case 3:
                    Console.WriteLine("Wednesday's are 50% off taxes; your total comes to: " + CalculateSum(userInput1, tax: .15).ToString("C"));
                    break;

            }
            Console.ReadLine();
        }

        static int promptItems()
        {
            Console.WriteLine("Shirts are originally $20 at a 30% tax per shirt.");
            Console.Write("Enter the number of shirts you want to buy you want to buy>>>");
            return Convert.ToInt32(Console.ReadLine());
        }

        static double CalculateSum(double userInput1, double cost = 20, double tax = .30)
        {
            double total = userInput1 * cost;
            return total + (total * tax);
        }
    }
}
