﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountVowels
{
    class Program
    {
        static void Main(string[] args)
        {
            int vowel = 0;
            WriteLine(" Enter a word or phrase");
            string text = ReadLine();

            for (int x = 0; x < text.Length; x++)
            {
                string scan = text.Substring(x, 1).ToLower();
                if (scan == "a" || scan == "e" || scan == "i" || scan == "o" || scan == "u")
                    vowel++;
            }


            WriteLine(" There are {0} vowels in your word", vowel);
            ReadLine();

        }
    }
}
