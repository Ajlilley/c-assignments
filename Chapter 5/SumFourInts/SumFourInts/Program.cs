﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumFourInts
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            int number;
            int limit =0;
            int sum = 0;

            do
            {
                Write("Enter in an integer: ");
                input = ReadLine();
                number = Convert.ToInt32(input);
                sum += number;
                limit = limit + 1;
            } while (limit < 4);
            WriteLine("The sum of your integers is " + sum);
            ReadLine();
        }
    }
}
