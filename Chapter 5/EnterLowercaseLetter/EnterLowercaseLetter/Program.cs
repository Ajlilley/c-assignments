﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterLowercaseLetter
{
    class Program
    {
        static void Main(string[] args)
        {
            char c;
            while ((c = Console.ReadKey(true).KeyChar) != '!')
            {
                if (char.IsLower(c))
                {
                    Console.WriteLine("OK");
                    continue;
                }
                Console.WriteLine("The character is not a lowercase letter.");
            }

        }
    }
}
