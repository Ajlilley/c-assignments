﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuiltiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Enter a integer, 1-10 >> ");
            int number = Convert.ToInt32(ReadLine());

            for (int x = 1; x <= 10; x++)
                WriteLine("{0} X {1} = {2} ", x, number, x * number);

            ReadLine();

        }
    }
}
