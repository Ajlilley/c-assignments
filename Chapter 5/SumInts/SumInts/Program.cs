﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumInts
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0;
            int userInputInt = 0;

            while(userInputInt != 999)
            {
                Console.Write("Enter an integer: ");
                userInputInt = Convert.ToInt32(Console.ReadLine());
                if(userInputInt !=999)
                {
                    total += userInputInt;
                }
            }
            Console.WriteLine("");
            Console.WriteLine("Your total is: " + total);
            Console.ReadLine();
        }
    }
}
