﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TippingTable3
{
    class Program
    {
        static void Main(string[] args)
        {
            double dinnerPrice = 0;
            double tipRate;
            double tip;


            double lowRate;
            Write(" Enter the lowest rate >> ");
            lowRate = Convert.ToDouble(ReadLine());
            double maxRate;
            Write(" Enter the highest rate >> ");
            maxRate = Convert.ToDouble(ReadLine());
            double lowDinnerPrice;
            Write(" Enter the lowest dinner price >> ");
            lowDinnerPrice = Convert.ToDouble(ReadLine());
            double highDinnerPrice;
            Write("Enter the highest dinner price >> ");

            highDinnerPrice = Convert.ToDouble(ReadLine());
            const double TIPSTEP = 0.05;
            const double DINNERSTEP = 2.50;


            Write("Price");
            for (tipRate = lowRate; tipRate <= maxRate; tipRate += TIPSTEP)
                Write("{0,8}%", tipRate.ToString("F"));
            WriteLine();
            WriteLine("---------------------------------------------------------------------------------------------------------------------------------");
            WriteLine();

            tipRate = lowRate;
            while (lowDinnerPrice <= highDinnerPrice)
            {

                Write("{0,8}", lowDinnerPrice.ToString("C"));
                while (tipRate <= maxRate)
                {
                    tip = lowDinnerPrice * tipRate;
                    Write("{0,8}", tip.ToString("F"));
                    tipRate += TIPSTEP;
                }
                lowDinnerPrice += DINNERSTEP;
                tipRate = lowRate;
                WriteLine();
            }

            ReadLine();
        }
    }
}
