﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ranNumberGenerator = new Random();
            int randomNumber;
            randomNumber = ranNumberGenerator.Next(1, 11);
            WriteLine(randomNumber);
            int guess = 0;


            while (guess != randomNumber)
            {
                Write(" Guess a random number 1 through 10 >> ");
                guess = Convert.ToInt32(ReadLine());
                if (guess < randomNumber)
                    Write(" Your guess is too low!!");
                if (guess > randomNumber)
                    Write(" Your guess is too high!!");

            }
            WriteLine(" Your guess was right on the money!!");
            ReadLine();

        }
    }
}
