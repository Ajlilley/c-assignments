﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sum500
{
    class Program
    {
        static void Main(string[] args)
        {
            int value = 500;
            int x;
            int sum = 0;

            for (x = 1; x <= value; ++x)
            {
                sum += x;
            }
            WriteLine("The sum of all number 1-500 is " + sum);
            ReadLine();

        }
    }
}
