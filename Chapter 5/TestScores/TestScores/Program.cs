﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestScores
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            int count = 0;
            double average = 0;

            Console.WriteLine("Enter all your exam scores one by one. When finished, enter -99");

            double scores = Convert.ToDouble(Console.ReadLine());

            while (scores != -99 && scores >= 0 && scores <= 100)
            {
                sum += scores;
                count++;
                scores = Convert.ToDouble(Console.ReadLine());
                continue;
            }
            if (scores < 0 || scores > 100)
            {
                Console.WriteLine("Invaild score! Try again!");
                Console.ReadLine();
            }

            if (scores == -99)
            {
               average = sum / count;
               Console.WriteLine("You entered a total of " + count + " scores, with an average of " + average.ToString("f2"));
               Console.ReadLine();
            }
        }
    }
}
