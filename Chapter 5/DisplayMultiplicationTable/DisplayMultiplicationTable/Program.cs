﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayMultiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int val = 10;


            for (int x = 1; x <= val; ++x)
                Write("\t{0, 0}", x);

            WriteLine();
            WriteLine();

            for (int row = 1; row <= val; ++row)
            {
                Write("{0, -8}", row);
                for (int column = 1; column <= val; ++column)
                {
                    Write("{0, -8}", row * column);
                }
                WriteLine();
            }
            ReadLine();
        }
    }
}
