﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHockeyPlayer
{
    class Program
    {
        static void Main(string[] args)
        {
            HockeyPlayer hockeyPlayer;
            hockeyPlayer = new HockeyPlayer();

            hockeyPlayer.PlayerName = "Vladimir Tarasenko";
            hockeyPlayer.JerseyNumber = 91;
            hockeyPlayer.GoalsScored = 40;
            Console.WriteLine("The players name is  {0}.\nHis jersey number is {1}.\nThe nmber of goals he scored this season was {2}.", hockeyPlayer.PlayerName, hockeyPlayer.JerseyNumber, hockeyPlayer.GoalsScored);
            Console.ReadLine();
        }
    }
}
