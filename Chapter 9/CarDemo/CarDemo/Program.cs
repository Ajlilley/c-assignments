﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Car c1 = new Car("Subaru", 65);
            Car c2 = new Car("Ford");

            Console.WriteLine(c1);
            Console.WriteLine();
            Console.WriteLine(c2);
            Console.WriteLine();

            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine();
            c1++;
            Console.WriteLine(c1);
            Console.WriteLine();
            c2++;
            Console.WriteLine(c2);
            Console.ReadLine();
        }
    }
}
